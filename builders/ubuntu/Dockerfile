FROM ubuntu:focal AS build

# Install build tools and development libraries.
RUN apt-get -y update; \
    DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata; \
    apt-get -y install \
		build-essential \
		libsdl2-dev \
		libsdl2-image-dev \
		libsdl2-ttf-dev \
		libogg-dev \
		libvorbis-dev \
        curl
# Note about tzdata install: we have to explicitly set the noninteractive
# frontend to the package manager, or otherwise it will hold up the GitLab
# runner indefinitely waiting for you to tell it what time zone you're in.

WORKDIR /build-slice

ARG OPTIMIZATIONS

# Build third-party libraries and install them.
COPY third-party third-party
COPY third-party-Makefile .
RUN make -f third-party-Makefile -j$(nproc) install

# Build Slice libraries and install them.
COPY include include
COPY src src
COPY Makefile .
RUN make -j$(nproc) install



FROM ubuntu:focal

# Install build tools and development libraries.
RUN apt-get -y update; \
    DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata; \
    apt-get -y install \
		build-essential \
		libsdl2-dev \
		libsdl2-image-dev \
		libsdl2-ttf-dev \
		libogg-dev \
		libvorbis-dev \
        curl

COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/include /usr/local/include
COPY --from=build /usr/local/bin /usr/local/bin

WORKDIR /game/built
COPY default-content .

WORKDIR /game
