1.1.0.4
 - No longer cycles once when quit is requested.

1.1.0.3
 - Made text half its previous height.

1.1.0.2
 - Modifications to adapt to changes to slBox type in Slice 2.6.0.0.

1.1.0.1
 - Fixed a bug in `SimpleLogo` which caused the logos to flash during the first rendered frame at 100% opacity.

1.1.0.0
 - Logos are now expected to be within a "logos" subdirectory rather than just beside the executable, for improved organization.
 - Generalized GlassBladeLogo into CustomLogo and made macro GlassBladeLogo which passes hard-coded values to CustomLogo for showing the GlassBlade logo.
 - Also added macros SDLsLogo() and Box2DLogo() which, respectively, show SDL's logo and Box2D's logo.
 - Added macro AllLogos(box2d) which conveniently calls GlassBladeLogo, SliceLogo, and SDLsLogo, and if `box2d` is true, also calls Box2DLogo.
 - Refined scaling behavior of the logo caption for particularly wide-screen resolutions.

1.0.0.0
 - Library created. 
   - The logo sequence functions are blocking calls and will terminate either after the logo sequence has finished or `OnSkipLogo` has been called.
   - If you want the user to able to skip either sequence, set up the desired keybind to call `OnSkipLogo`.
   - For the `GlassBladeLogo` sequence to work properly, "GlassBlade.png" (found in "/dependencies" within package) must be in the same directory as the executable, and its filename must be preserved.
   - For either logo sequence to work properly, the Slice default font path must be valid.