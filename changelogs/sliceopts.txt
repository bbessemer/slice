1.3.1.0
 - In accordance with Slice 2.8.0.0, uses the "listening" API to update the key binds menu.
 - Uses slDescribeKeyInfo instead of re-implementing that functionality itself.

1.3.0.9
 - Internal modifications to conform to the Slice 2.6.9.0 list API changes.

1.3.0.8
 - Added opSetVisibilityCallback. A visibility callback allows the application to be notified when the options menu opens or closed. This can be particularly useful for using SliceOpts in conjunction with Slice3D and SDL's relative mouse mode.

1.3.0.7
 - Compliance with Slice 2.6.6.0, no more direct modification of an slBox's texture reference.

1.3.0.6
 - Some modifications to comply with Slice 2.6.0.0 changes to slBox properties now using slVec2 type.

1.3.0.5
 - Named allocators and lists to comply with Slice 2.5.7.3.
 - opInit once again calls scrInit, and now opQuit also calls scrQuit. This works because of the init-counting mechanism present since SliceScroll version 1.0.1.2.

1.3.0.4
 - opInit no longer calls scrInit. This means you must explicitly call scrInit before calling opInit.
   - This is because scrQuit now exists. Previously, scrInit was safe to call multiple times without a corresponding call to scrQuit each time, since all it did was ask Slice for some keybinds. However, having opInit implictly call scrInit means that the app developer likely will not notice that there is no call to scrQuit, since there is no call to scrInit either.
     - The solution is not to call scrQuit in opQuit because, if the app developer uses scrScroll objects directly in the app, or another extension uses them, then opQuit would cause undesired behavior by causing the deletion of any remaining scrScroll objects, which would cause problems if the developer called opQuit and then attempted to continue manipulating references to scrScroll objects that had inadvertently been freed.
   - Also, scrInit is no longer safe to call multiple times without a corresponding call to scrQuit each time as it used to be, since now it also initializes a garbage collection action.
 - To reflect the new garbage collection strategy and block allocator manipulation functions in Slice 2.5.7.0, now hooks a garbage collection action which frees empty blocks in the scrScroll allocator.

1.3.0.3
 - Began adding customization options for adjusting the colors of various items in the menu.

1.3.0.2
 - Made the main options menu panel fill the entire screen.

1.3.0.1
 - Tiny modification to UI creation code.
 - Added comments in sliceopts.h informing of the z space of boxes making up the options menu.

1.3.0.0
 - The keybindings menu has been overhauled to take advantage of the new "Slice-Scroll" official Slice extension. This means it looks better and also has a proper scroll bar now, and those big ugly arrow buttons are no more. As such, the image assets for those big ugly arrow buttons also are no more in the "dependencies" folder of the Slice distribution, with "tick-up.png" and "tick-down.png" being their spiritual successors, used by the Scroll extension.

1.2.1.1
 - Slice 2.0.2.0 introduced a better box hover selection algorithm that keeps obscured boxes from being hovered over if a hoverable box is in front of them. (This can be thought of as if a ray is cast from the front to the back of the screen, and the first hoverable box is the one shown as hovering.) In order to block any hoverable boxes behind the SliceOpts menu panels, which are translucent, from being drawn as hovered over, the primary menu panel and the keybinds menu panel have been made hoverable, with their hovering visual details set to the same as their normal visual details. So they don't appear to hover, they just prevent visual artifacts from occurring.