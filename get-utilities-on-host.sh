#!/bin/sh
set -eux

docker build . -f builders/ubuntu/Dockerfile -t slice-builder-ubuntu

docker rm slice-builder-ubuntu || true # Just in case it wasn't removed already,
# for example due to script failure caused by permission issues when copying.

docker create --name slice-builder-ubuntu slice-builder-ubuntu
docker cp slice-builder-ubuntu:/usr/local/bin/. /usr/local/bin
docker rm slice-builder-ubuntu
