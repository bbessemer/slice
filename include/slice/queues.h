#include <slice.h>



class slAsyncNonblockingQueue
{
    SDL_mutex* sync;
    int offsetof_next;
    void* first = NULL;
    void** last_ptr = &first;

public:

    slAsyncNonblockingQueue (int offsetof_next);
    void Init ();
    void Quit ();

    void Push (void* item); // Do not pass NULL to this.
    void* Get (); // Will immediately return NULL if there is nothing.
};

class slAsyncBlockingQueue
{
    slAsyncNonblockingQueue nb_queue;
    SDL_sem* waiting;

public:

    slAsyncBlockingQueue (int offsetof_next);
    void Init ();
    void Quit ();

    void Push (void* item); // Push(NULL) will safely post to the semaphore.
    void* Get (); // Will wait until a corresponding Push has occurred.
};

class slConsumerQueue
{
    slAsyncBlockingQueue queue;
    int consumer_count;
    SDL_Thread** consumer_threads;

public:

    slConsumerQueue (int offsetof_next, int consumer_count = 1);
    ~slConsumerQueue ();
    void Init ();
    void Quit ();

    void Push (void* item); // Do not pass NULL to this.

private:

    // Allow setup & cleanup around the loop.
    virtual void PreLoop ();
    virtual void PostLoop ();
    virtual void ItemFunc (void* item) = 0;

    void Consume ();
    static void Consume_Wrap (slConsumerQueue* self);
};
