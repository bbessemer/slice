#pragma once

// Must define this now so it's defined when slTreeMap.h is included by slice.h
struct slTreeNodeData
{
    void* left;
    void* right;
};

#include <slice.h>

struct slTreeSet
{
    void* root = NULL;
    void Insert (void* item);
    void Remove (void* item);
    void* Lookup (void* item);

    void PrintInOrder (void* local_root, int depth, char pre);
    inline void PrintInOrder () slForceInline
    {
        PrintInOrder(root,0,0xC4);
    }

    int total_items = 0;
    int Recount (void* local_root);
    inline int Recount () slForceInline
    {
        return Recount(root);
    }

    /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */
    virtual int Compare (void* item_a, void* item_b) = 0;
    virtual slTreeNodeData* GetTreeData (void* item) = 0;
    virtual void PrintItem (void* item) = 0;
};
