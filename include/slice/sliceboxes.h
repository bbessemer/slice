#ifndef SLICEBOXES_H_INCLUDED
#define SLICEBOXES_H_INCLUDED

namespace sl_internal
{
	struct slBoxRender
	{
		/// This is a cache. It is not intended to be used outside the engine.
		bool draw_anything,draw_back,draw_texture,draw_texborder,draw_border,color_setup_texborder,color_setup_boxborder;
		slTexture* texref;
		GLrgba_f backcolor, bordercolor, texbordercolor, drawmask;
		GLxywh box_xywh, tex_xywh;
		GLvec2 rotpoint;
		GLfloat cosvalue,sinvalue;
	};
};
struct slBox
{
	// The unions with simple slTexture* were removed because you are no longer supposed to directly modify texref or hovertexref.
	// Use the swap chain system so that ref counts are properly updated.
	slTexSwapChain texref_swapchain;
	slTexSwapChain hovertexref_swapchain;

	void* userdata;
	void (*onclick) (slBox*);
	void (*onrightclick) (slBox*);
	slBU _index_;
	slScalar rotangle; // Used only if `rot` is true
	slVec2 xy,wh,tex_wh,rotpoint;
	// `rotpoint` used only if `rotcenter` is false
	SDL_Color backcolor,bordercolor,hoverbackcolor,hoverbordercolor,/*gapcolor,*/texbordercolor,hovertexbordercolor;
	SDL_Color drawmask,hoverdrawmask;
	bool rot;
	bool rotcenter;
	bool hoverable;
	bool maintainaspect;
	bool visible;

	Uint8 z; // Lower is Closer
	Uint8 tex_align_x : 2, tex_align_y : 2;
	Uint8 aspect_align_x : 2, aspect_align_y : 2;
	Uint8 border_thickness;

	sl_internal::slBoxRender render;
	SDL_Rect* scissor; // NULL for whole screen.



	SDL_Rect CalcScreenPos (); // Doesn't support rotation, is intended for usage as a scissor rect. As such, bottom left is 0,0 (y inverted from slBox coordinates convention).
	inline void SetTexRef (slTexRef texref) slForceInline
	{
		slQueueTexSwap(&texref_swapchain,texref);
	};
	inline void SetHoverTexRef (slTexRef hovertexref) slForceInline
	{
		slQueueTexSwap(&hovertexref_swapchain,hovertexref);
	};
	inline void SetDims (slVec2 xy, slVec2 wh, Uint8 z) slForceInline
	{
		this->xy = xy;
		this->wh = wh;
		this->z = z;
	};
	inline void SetRots (bool rot = false, slScalar rotangle = 0, bool rotcenter = true, slVec2 rotpoint = 0) slForceInline
	{
		this->rot = rot;
		this->rotangle = rotangle;
		this->rotcenter = rotcenter;
		this->rotpoint = rotpoint;
	};
	inline void AspectAnchor (Uint8 tex_align_x, Uint8 tex_align_y) slForceInline
	{
		maintainaspect = true;
		this->tex_align_x = tex_align_x;
		this->tex_align_y = tex_align_y;
	};
	void PutWithin (slBox* outer); // Does not (yet) take rotation into account.
};
void slBoxHoverAbsorb (slBox* box);
#define slBlankColor {0,0,0,0}
slBox* slCreateBox ();
slBox* slCreateBox (slTexRef tex);
slBox* slCreateBox (slTexRef tex, slTexRef hovertex);
void slDestroyBox (slBox* todel);
bool slPointOnBox (slBox* box, slVec2 point);
inline void slSetBoxDims (slBox* box, slVec2 pos, slVec2 dims, Uint8 z) { box->SetDims(pos,dims,z); }; // Deprecated
inline void slSetBoxDims (slBox* box, slScalar x, slScalar y, slScalar w, slScalar h, Uint8 z) { box->SetDims(slVec2(x,y),slVec2(w,h),z); }; // Very Deprecated
inline void slSetBoxRots (slBox* box, bool rot = false, slScalar rotangle = 0, bool rotcenter = true, slVec2 rotpoint = slVec2(0)) { box->SetRots(rot,rotangle,rotcenter,rotpoint); }; // Deprecated
inline void slRelBoxDims (slBox* box, slBox* rel) { box->PutWithin(rel); }; // Deprecated
struct slCorners
{
	//slVec2 p00,p01,p10,p11;
	slScalar p00x,p00y,p01x,p01y,p10x,p10y,p11x,p11y;
};
slCorners slGetBoxCorners (slBox* item);

void slDrawUI ();
	// If you define an AppDrawStage, you must call this in it somewhere to draw the UI (this allows you to choose to draw things after UI).
	// If an AppDrawStage is not defined, slDrawUI will be called directly instead, as a default.



slBox* slGetHoveredBox (slVec2 cursor);
namespace sl_internal
{
	void slInitUI ();
	void slQuitUI ();
	bool slOnClickUI (slVec2 cursor, bool leftclick);
	void slRenderPrepareAll (slBox* hoverbox);
};
void slBindFullscreenArray ();

#endif // SLICEBOXES_H_INCLUDED
