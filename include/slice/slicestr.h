#ifndef SLICESTR_H_INCLUDED
#define SLICESTR_H_INCLUDED

slBU slStrLen (char* str);
bool slStrEq (char* str1, char* str2);

char* slStrClone (char* from); // deprecated
char* slStrAlwaysClone (char* from);
char* slStrConcat (char* first, char* second); // deprecated
char* slStrAlwaysConcat (char* str1, char* str2);
/*
    Difference between the new "Always" versions and their older analogs:

    Old versions null-check their arguments, new versions assume you have not
    passed NULL to them unless otherwise specified.

    Old versions will return NULL to represent zero-length strings, whereas new
    versions will *always* (hence the names) allocate the same way whether the
    output length is zero or nonzero.
*/

char* slStrChopLast (char* str);
char* slStrChopFirst (char* str);
void slStrChopLast_Replace (char** str);
void slStrChopFirst_Replace (char** str);
char* slStrAppendAfter (char* str, char cc);
char* slStrAppendBefore (char* str, char cc);
void slStrAppendAfter_Replace (char** str, char cc);
void slStrAppendBefore_Replace (char** str, char cc);

void slStrMakeLower (char* str);
void slStrMakeUpper (char* str);

// All the number scan functions can safely handle NULL or an empty string.
slBU slScanHexU_Fast (char* str);
slBS slScanHexS_Fast (char* str);
slBU slScanDecU_Fast (char* str);
slBS slScanDecS_Fast (char* str);
slScalar slScanDecF_Fast (char* str);

// These return true if the input is invalid, false if there were no issues.
// If the return is true, the memory at `putin` has not been touched.
bool slScanHexU (char* str, slBU* putin);
bool slScanHexS (char* str, slBS* putin);
bool slScanDecU (char* str, slBU* putin);
bool slScanDecS (char* str, slBS* putin);
bool slScanDecF (char* str, slScalar* putin);

bool slGetFileSegment (FILE* file, char** into, char separator); // Returns true: more data remain, false: end of file reached.
#define slGetFileLine(file,into) slGetFileSegment(file,into,'\n')

#endif // SLICESTR_H_INCLUDED
