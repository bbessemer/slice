#ifndef SLICETEXTURES_H_INCLUDED
#define SLICETEXTURES_H_INCLUDED

struct slFont
{
	TTF_Font* font;
	char* path;
};
slFont* slLoadFont (char* path);
char* slGetDefaultFontPath ();
void slSetDefaultFontPath (char* path);



#define slTextures slTexture::Textures
namespace sl_internal { void slTexturesQuit (); }
class slTexture
{
    slBU _index_; // in slTextures list... AKA slTexture::Textures list.
    SDL_atomic_t usages = {0}; // If this is zero, the garbage collector will free the texture.
    static slList Textures;

protected:
    slTexture ();
    virtual ~slTexture ();
    friend void slReleaseTexture (slTexture* tex);

    virtual void Unregister (); // Happens before (separately from) destruction.
    friend void slDoomTexture (slTexture* tex);
    friend void sl_internal::slTexturesQuit ();

public:
	slScalar aspect; // Only is valid is dims_valid is true. If texture can't be loaded, dims_valid will still become true and this will be set to 1.
    GLuint tex;
	volatile bool ready = false; // Becomes true once texture is completely uploaded to OpenGL.
	volatile bool dims_valid = false; // Becomes true as soon as "aspect" is valid, significantly before "ready" is true.

    inline void WaitReady () slForceInline
    {
        // Useful if you want to delay rendering a frame until a custom texture is uploaded.
        while (!ready) SDL_Delay(0);
    }
    inline slScalar WaitDimsValid () slForceInline
    {
        // Useful if you want to safely use the 'aspect' value in some calculations.
        while (!ready) SDL_Delay(0);
        return aspect; // So that you can do 'aspect = texture->WaitDimsValid();' on one line.
    }

	inline void Reserve () slForceInline // Reserve once.
	{
		SDL_AtomicIncRef(&usages);
	};
	void Abandon (); // Abandon from one reservation.
};
class slTexRef
{
public:
    slTexture* raw_ref;
//public:
    inline slTexRef (slTexture* raw_ref) slForceInline
    {
        this->raw_ref = raw_ref;
        raw_ref->Reserve();
    }
    inline slTexRef (const slTexRef& copy_from) slForceInline
    {
        raw_ref = copy_from.raw_ref;
        raw_ref->Reserve();
    }
    inline ~slTexRef () slForceInline
    {
        raw_ref->Abandon();
    }
    inline slTexture* ReserveGetRaw () slForceInline
    {
        raw_ref->Reserve();
        return raw_ref;
    }
    /*inline slTexture* AccessRaw () slForceInline
    {
        return raw_ref;
    }*/
};



struct slTextInfo
{
    char* text;
    slFont* font;
    SDL_Color text_color, back_color/*unused, for now*/;
    inline slTextInfo (char* text, slFont* font, SDL_Color text_color, SDL_Color back_color) slForceInline
    {
        this->text = text;
        this->font = font;
        this->text_color = text_color;
        this->back_color = back_color;
    }
};
struct slTextTexRef;
class slTextTexture : public slTexture
{
    slTextInfo text_tree_key;
    slTreeNodeData text_tree_data;
    friend class slTextTexturesTreeProto;

    slTextTexture* next_load;
    friend class slTextAsyncQueueProto;

    slTextTexture (slTextInfo search_key);
    ~slTextTexture () override;

public:
    void Unregister () override;

    static slTextTexRef Render (
        char* str,
        SDL_Color color = {255,255,255,255},
        slFont* font = slLoadFont(slGetDefaultFontPath())
    );
};
struct slTextTexRef : public slTexRef
{
	slTextTexRef (slTextTexture* raw_ref) : slTexRef(raw_ref) {};
};
inline slForceInline slTextTexRef slRenderText (
    char* str,
    SDL_Color color = {255,255,255,255},
    slFont* font = slLoadFont(slGetDefaultFontPath())
)
{
    return slTextTexture::Render(str,color,font);
}



struct slImageInfo
{
    char* loaded_from;
    inline slImageInfo (char* loaded_from) slForceInline
    {
        this->loaded_from = loaded_from;
    }
};
struct slImageTexRef;
class slImageTexture : public slTexture
{
    slImageInfo image_tree_key;
    slTreeNodeData image_tree_data;
    friend class slImageTexturesTreeProto;

    slTextTexture* next_load;
    friend class slImageAsyncQueueProto;

    slImageTexture (slImageInfo search_key);
    ~slImageTexture () override;

public:
    void Unregister () override;

    static slImageTexRef Load (char* path);
};
struct slImageTexRef : public slTexRef
{
	slImageTexRef (slImageTexture* raw_ref) : slTexRef(raw_ref) {};
};
inline slForceInline slImageTexRef slLoadTexture (char* path)
{
    return slImageTexture::Load(path);
}



struct slMemoryTexRef;
class slMemoryTexture : public slTexture
{
public:
    void Update (slInt2 wh, void* colorbuf);

    static slMemoryTexRef Create ();
};
struct slMemoryTexRef : public slTexRef
{
	slMemoryTexRef (slMemoryTexture* raw_ref) : slTexRef(raw_ref) {};

    inline void Update (slInt2 wh, void* colorbuf) slForceInline
        { ((slMemoryTexture*)raw_ref)->Update(wh,colorbuf); }
};
inline slForceInline slMemoryTexRef slCreateCustomTexture (slBU w, slBU h, void* proto_data)//, bool alpha, Uint8 row_alignment)
{
    slMemoryTexRef ref_out = slMemoryTexture::Create();
    ref_out.Update(slInt2(w,h),proto_data);
    return ref_out;
}
inline slForceInline slMemoryTexRef slSolidColorTexture (SDL_Color color)
{
	return slCreateCustomTexture(1,1,&color);
};



/** Do not attempt to create or use any slTargetTexture off the main thread. **/
struct slTargetTexRef;
class slTargetTexture : public slTexture
{
    slInt2 dims;
    GLuint framebuffer;
    GLuint depth_renderbuffer;
    GLenum draw_buffers [1] = {GL_COLOR_ATTACHMENT0};

    slTargetTexture (slInt2 wh);
    ~slTargetTexture ();
    void InternalCreate();
    void InternalDestroy();

public:
    void DrawTo ();
    inline slInt2 GetDims () slForceInline { return dims; }

    static slTargetTexRef Create (slInt2 wh); // Canvas size: (wh.w, wh.h) pixels.
    static slTargetTexRef Create (); // Canvas size: (screen.x, screen.y) pixels.
    static slTargetTexRef Create (slVec2 wh); // Canvas size: (screen.x * wh.w, screen.y * wh.h) pixels.
    void Resize (slInt2 wh); // Canvas size: (wh.w, wh.h) pixels.
    void Resize (); // Canvas size: (screen.x, screen.y) pixels.
    void Resize (slVec2 wh); // Canvas size: (screen.x * wh.w, screen.y * wh.h) pixels.
};
struct slTargetTexRef : public slTexRef
{
	slTargetTexRef (slTargetTexture* raw_ref) : slTexRef(raw_ref) {};

    inline void DrawTo () slForceInline
        { ((slTargetTexture*)raw_ref)->DrawTo(); }
    inline slInt2 GetDims () slForceInline
        { return ((slTargetTexture*)raw_ref)->GetDims(); }
    inline void Resize (slInt2 wh) slForceInline // Canvas size: (wh.w, wh.h) pixels.
        { ((slTargetTexture*)raw_ref)->Resize(wh); }
    inline void Resize () slForceInline // Canvas size: (screen.x, screen.y) pixels.
        { ((slTargetTexture*)raw_ref)->Resize(); }
    inline void Resize (slVec2 wh) slForceInline // Canvas size: (screen.x * wh.w, screen.y * wh.h) pixels.
        { ((slTargetTexture*)raw_ref)->Resize(wh); }
};
void slDrawToScreen ();



void slQueueTexUpload (slTexture* tex, SDL_Surface* surf);
SDL_Surface* slLoadTexture_Surf (char* path); /// May return NULL.
SDL_Surface* slRenderText_Surf (char* str, SDL_Color color, slFont* font); /// May return NULL.
SDL_Surface* slConvertSurface_RGBA32 (SDL_Surface* original); /// Don't pass NULL as `original`. May return NULL.
SDL_Surface* slConvertSurface_RGBA32_Free (SDL_Surface* original); /// Convenience function, frees `original`. Safe to pass NULL as `original` to this function.
//#define slColorMatch(color1,color2) (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
#define slColorMatch(color1,color2) (*(Uint32*)&(color1) == *(Uint32*)&(color2))



namespace sl_internal
{
	void slTexturesInit ();
	void slTexturesQuit ();
};



struct slTexReplacement
{
	slTexture* replacement;
	slTexReplacement* next;
};

struct slTexSwapChain
{
	slTexture* texref;
	slTexReplacement* first;
};
void slQueueTexSwap (slTexSwapChain* chain, slTexRef to_swap_ref); // Queue a swap to a texture as soon as it is ready.

// You can use these functions for extensions etc.:
void slStepTexSwapChain (slTexSwapChain* chain); // If there are replacements queued, swaps in the most recent ready texture.
//bool slTexInSwapChain (slTexSwapChain* chain, slTexture* tex); // Use this for garbage collection (you should not GC a texture that will be swapped in!).
// ^ No longer a thing, because the swap chain increments and decrements the textures' `usages` counter.
void slQuitTexSwapChain (slTexSwapChain* chain); // Free any queued swaps prior to destroying the object the chain is a part of.
slTexSwapChain slInitTexSwapChain (); // Initializes the slTexSwapChain structure.



#endif // SLICETEXTURES_H_INCLUDED
