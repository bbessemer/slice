#pragma once

void OnSkipLogo (); // Set a keybind such as Escape to call this function when pressed.
//void GlassBladeLogo (); // Run the GlassBlade logo sequence.
//void SDLsLogo ();

void SimpleLogo (char* imgpath, char* caption);

#define GlassBladeLogo() SimpleLogo("logos/GlassBlade.png","Glass Blade Games LLC")
void SliceLogo (); // Run the Slice logo sequence.
#define SDLsLogo() SimpleLogo("logos/SDL.png","Uses SDL, SDL_image, & SDL_ttf.")
#define Box2DLogo() SimpleLogo("logos/Box2D.png","Uses Box2D Physics")
#define Bullet3DLogo() SimpleLogo("logos/Bullet3D.png","Uses Bullet3D Physics")

#define DefaultLogoSequence(box2d,bullet3d)\
{\
	GlassBladeLogo();\
	SliceLogo();\
	SDLsLogo();\
	if (box2d) Box2DLogo();\
	if (bullet3d) Bullet3DLogo();\
}
