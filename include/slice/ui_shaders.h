#pragma once


struct slShaderProgram
{
	GLuint vertshader;
	GLuint fragshader;
	GLuint program;
};
#define slLocateUniform(shaderprogram,varname) glGetUniformLocation(shaderprogram.program,varname)
slShaderProgram slCreateShaderProgram (char* name, char* vert_src, char* frag_src, void (*bind_attribs) (GLuint program));
void slDestroyShaderProgram (slShaderProgram program);

namespace sl_internal
{
	extern slShaderProgram slColorRotateProgram, slTexRotateProgram;
	extern GLuint
		slColorRotateProgram_XYWH,
		slColorRotateProgram_RotPoint,
		slColorRotateProgram_SinValue,
		slColorRotateProgram_CosValue,
		slColorRotateProgram_WindowAspect;
	extern GLuint
		slColorProgram_Color,
		slColorRotateProgram_Color;
	extern GLuint
		slTexRotateProgram_XYWH,
		slTexRotateProgram_RotPoint,
		slTexRotateProgram_SinValue,
		slTexRotateProgram_CosValue,
		slTexRotateProgram_WindowAspect;
	extern GLuint
		slTexProgram_Tex,
		slTexProgram_Mask,
		slTexRotateProgram_Tex,
		slTexRotateProgram_Mask;
	void slSetDrawTexture (slTexture* texref);
	void slInitBoxRenderPrograms ();
	void slQuitBoxRenderPrograms ();
};
