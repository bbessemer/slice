#pragma once
#include <slice.h>

#include <slice3d/load-obj.h> // just for objColor definition



typedef slScalar s3dRawVec3 __attribute__((vector_size(sizeof(slScalar)*4)));
struct GLvec4;
struct s3dVec3
{
	union
	{
		s3dRawVec3 xyz;
		struct
		{
			slScalar x,y,z;
		};
	};
	s3dVec3 () slForceInline : xyz((s3dRawVec3){0,0,0}) {};
	s3dVec3 (slScalar x, slScalar y, slScalar z) slForceInline : x(x), y(y), z(z) {};
	s3dVec3 (slScalar xyz) slForceInline : x(xyz), y(xyz), z(xyz) {};
	static s3dVec3 from_float_array (float* rawfloats) slForceInline
	{
		s3dVec3 out;
		out.xyz = (s3dRawVec3)
		{
			*rawfloats,
			rawfloats[1],
			rawfloats[2]
		};
		return out;
	};
	s3dVec3 operator+ (slScalar other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz += other;
		return out;
	};
	s3dVec3 operator- (slScalar other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz -= other;
		return out;
	};
	s3dVec3 operator* (slScalar other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz *= other;
		return out;
	};
	s3dVec3 operator/ (slScalar other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz /= other;
		return out;
	};
	s3dVec3 operator+= (slScalar other) slForceInline
	{
		this->xyz += other;
		return *this;
	};
	s3dVec3 operator-= (slScalar other) slForceInline
	{
		this->xyz -= other;
		return *this;
	};
	s3dVec3 operator*= (slScalar other) slForceInline
	{
		this->xyz *= other;
		return *this;
	};
	s3dVec3 operator/= (slScalar other) slForceInline
	{
		this->xyz /= other;
		return *this;
	};
	s3dVec3 operator+ (s3dVec3 other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz += other.xyz;
		return out;
	};
	s3dVec3 operator- (s3dVec3 other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz -= other.xyz;
		return out;
	};
	s3dVec3 operator* (s3dVec3 other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz *= other.xyz;
		return out;
	};
	s3dVec3 operator/ (s3dVec3 other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz /= other.xyz;
		return out;
	};
	s3dVec3 operator+= (s3dVec3 other) slForceInline
	{
		this->xyz += other.xyz;
		return *this;
	};
	s3dVec3 operator-= (s3dVec3 other) slForceInline
	{
		this->xyz -= other.xyz;
		return *this;
	};
	s3dVec3 operator*= (s3dVec3 other) slForceInline
	{
		this->xyz *= other.xyz;
		return *this;
	};
	s3dVec3 operator/= (s3dVec3 other) slForceInline
	{
		this->xyz /= other.xyz;
		return *this;
	};
	s3dVec3 operator- ()
	{
		s3dVec3 out = *this;
		out.xyz = -out.xyz;
		return out;
	};
	inline s3dVec3 (GLvec4 from) slForceInline;
	slScalar length_squared () slForceInline
	{
		s3dVec3 squared = *this * *this;
		return squared.x + squared.y + squared.z;
	};
	slScalar length () slForceInline
	{
		return slsqrt(length_squared());
	};
	slScalar dist_squared (s3dVec3 other) slForceInline
	{
		return (other - *this).length_squared();
	};
	slScalar dist (s3dVec3 other) slForceInline
	{
		return slsqrt(dist_squared(other));
	};
	s3dVec3 normalized () slForceInline
	{
		slScalar len = length();
		if (len == 0) return s3dVec3(0,1,0);
		else return *this / len;
	};
};
typedef slScalar s3dRawQuaternion __attribute__((vector_size(sizeof(slScalar)*4)));
struct s3dQuaternion
{
	s3dRawQuaternion data;

};
struct Mat4
{
	slScalar data [16];
};
typedef GLfloat GLvec4_raw __attribute__((vector_size(sizeof(GLfloat)*4)));
#include <xmmintrin.h>
struct GLvec4
{
	union
	{
		struct { GLfloat x,y,z,w; };
		GLvec4_raw data;
	};
	GLvec4 () slForceInline {};
	GLvec4 (GLvec4_raw in) slForceInline : data(in) {};
	//GLvec4 (GLvec3 in) slForceInline : data(in.data) {w = 1;};
	GLvec4 (GLfloat x, GLfloat y, GLfloat z, GLfloat w) slForceInline : x(x), y(y), z(z), w(w) {};
	GLvec4 (GLfloat x, GLfloat y, GLfloat z) slForceInline : x(x), y(y), z(z), w(1) {};
	GLvec4 (s3dVec3 in) slForceInline : x(in.x), y(in.y), z(in.z), w(1)
	{
		/// Do this properly with an intrinsic later.
	};
	GLfloat sum4 () slForceInline
	{
		/// Use an MMX intrinsic for summing the four values... it will speed this up quite nicely.
		/// For now though I can't access the damn documentation.
		return x + y + z + w;
	};
	void print (); // Just prints the numbers to console, for debugging purposes.
};
inline slForceInline s3dVec3::s3dVec3 (GLvec4 from)
{
	x = from.x;
	y = from.y;
	z = from.z;
};
typedef GLfloat GLvec16_raw __attribute__((vector_size(sizeof(GLfloat)*16)));
struct GLmat4;
GLmat4 GLmat4_mul (GLmat4& a, GLmat4& b);
struct GLmat4
{
	union
	{
		GLfloat data [16];
		struct { GLvec4_raw row1,row2,row3,row4; };
		GLvec16_raw entire_vec; /// Use this for addition to take advantage of AVX-512 more easily.
	};
	GLmat4 operator* (GLmat4 second) slForceInline
	{
		return GLmat4_mul(*this,second);
	};
	GLmat4 operator*= (GLmat4 second) slForceInline
	{
		*this = GLmat4_mul(*this,second);
		return *this;
	};
	GLmat4 operator+ (GLmat4 second) slForceInline
	{
		GLmat4 out;
		out.entire_vec = entire_vec + second.entire_vec;
		return out;
	};
	GLmat4 operator+= (GLmat4 second) slForceInline
	{
		entire_vec += second.entire_vec;
		return *this;
	};
	GLvec4 operator* (GLvec4 vec)
	{
		return GLvec4
		(
			GLvec4(row1 * vec.data).sum4(),
			GLvec4(row2 * vec.data).sum4(),
			GLvec4(row3 * vec.data).sum4(),
			GLvec4(row4 * vec.data).sum4()
		);
	};
	void print (); // Just prints the numbers to console, for debugging purposes.
	GLmat4 transpose ();
	void transpose_in_place ();
};
inline GLmat4 GLmat4_identity () slForceInline;
GLmat4 GLmat4_identity ()
{
	GLmat4 out;
	out.entire_vec = (GLvec16_raw){1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
	return out;
};
GLmat4 GLmat4_scaling (GLvec4 scalevec);
GLmat4 GLmat4_xrotate (GLfloat rads);
GLmat4 GLmat4_yrotate (GLfloat rads);
GLmat4 GLmat4_zrotate (GLfloat rads);
GLmat4 GLmat4_offset (GLfloat x, GLfloat y, GLfloat z);
inline slForceInline GLmat4 GLmat4_offset (GLvec4 vec) // w component is ignored.
{
	return GLmat4_offset(vec.x,vec.y,vec.z);
};
GLmat4 GLmat4_ortho (GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat near_clip, GLfloat far_clip);
//GLmat4 GLmat4_ortho_depth (GLfloat near_z, GLfloat far_z);
GLmat4 GLmat4_perspective (GLfloat fov, GLfloat znear, GLfloat zfar); // fov is in degrees.



struct GLcolor3
{
    GLfloat r,g,b;
    GLcolor3 (GLfloat gray) slForceInline : r(gray), g(gray), b(gray) {};
    GLcolor3 (GLfloat r, GLfloat g, GLfloat b) slForceInline : r(r), g(g), b(b) {};
    /* TO DO: use vector math to speed up conversion from bytes to floats */
    GLcolor3 (SDL_Color rgb) slForceInline : r(rgb.r * slInv255), g(rgb.g * slInv255), b(rgb.b * slInv255) {};
    GLcolor3 (objColor obj) slForceInline : r(obj.r), g(obj.g), b(obj.b) {};
};

struct s3dMesh
{
    slTexture* ambient_texref;
	slTexture* diffuse_texref;
    slTexture* specular_texref;
    slTexture* opacity_texref;
    slTexture* normal_texref;
	//float* vertices;
	GLuint vertex_buffer;
	uint32_t triangle_count;

    GLcolor3 ambient_color, diffuse_color, specular_color;
    GLfloat specular_exponent;
    bool mag_filter_nearest; // if set to true, use GL_NEAREST as texture magnification filter. Otherwise use LINEAR.
    bool ignore_environment_ambient; // Do not modulate lighting by global color values. (Useful for televisions etc.)
};

struct s3dObject;
struct s3dModel
{
	s3dMesh* meshes;
	slBU _index_;
	uint32_t meshcount; // will be less than the true count during loading

	SDL_atomic_t usages;
	inline void Reserve () slForceInline // Reserve once.
	{
		SDL_AtomicIncRef(&usages);
	};
	void Abandon (); // Abandon from one reservation.
    void* factory_data;
    void (*factory_free) (s3dModel*);

    GLuint matrix_buffer;
    GLuint matrix_texture;
    slBU matrix_count;
    slBU matrix_residency_capacity; // capacity of s3dObject** buffer
    slBU matrix_buffer_capacity; // capacity of OpenGL matrix buffer
    s3dObject** matrix_residency;
    SDL_mutex* matrix_mutex; // prevents residency from getting corrupted

    void AddMatrix (s3dObject* add_obj);
    void RemoveMatrix (s3dObject* remove_obj);
    void UpdateMatrix (s3dObject* update_obj);
};
struct s3dModelRef
{
    s3dModel* raw_ref;
    inline s3dModelRef (s3dModel* raw_ref) slForceInline
    {
        this->raw_ref = raw_ref;
        raw_ref->Reserve();
    }
    inline s3dModelRef (const s3dModelRef& copy_from) slForceInline
    {
        raw_ref = copy_from.raw_ref;
        raw_ref->Reserve();
    }
    inline ~s3dModelRef () slForceInline
    {
        raw_ref->Abandon();
    }
    inline s3dModel* ReserveGetRaw () slForceInline
    {
        raw_ref->Reserve();
        return raw_ref;
    }
};
s3dModelRef s3dLoadObj (char* basepath, char* filename);
s3dModelRef s3dSpriteModel (objMaterial material = objFallbackMaterial);
s3dModelRef s3dSpriteModel (
    // Convenience function. These values are just used to make objMaterial
    // which is then passed to the other s3dSpriteModel function.
    char* sprite_texpath,
    bool mag_filter_nearest = true,
    float ambient = 0,
    float diffuse = 1,
    float specular = 0,
    float spec_exp = objFallbackMaterial.specular_exponent,
    bool ignore_environment_ambient = false
);
struct s3dCamera;
class s3dObject
{
    s3dModel* model; // Do NOT directly set this unless you manually deal with refcounts yourself!
	GLmat4 world_transform;
    slBU matrix_residence; // Where, in the s3dModel's buffer(s), is the matrix from this object?
    bool visible;

public:
	void SetModel (s3dModel* set_to);
    void SetWorldTransform (GLmat4 world_transform); // Combine origin, rotation, and scaling yourself however you wish.
    void SetVisible (bool visible);

    slBU _index_; // You shouldn't mess with this but it has to be public.

    friend s3dObject* s3dCreateObject (s3dModelRef model);
    friend void s3dDestroyObject (s3dObject* object);
    friend void s3dRender (slInt2 target_dims, s3dCamera cam);
    friend void s3dApplyWorldTransform (s3dObject* obj);
    friend class s3dModel;
};
s3dObject* s3dCreateObject (s3dModelRef model);
void s3dDestroyObject (s3dObject* object);

struct s3dCamera
{
	GLmat4 projection_matrix; // Slice3D compensates for aspect ratio on its own. Give a square-shaped projection here.
	GLmat4 final_matrix; // Don't touch. Calculated by Slice3D.
	s3dVec3 origin;
	//slVec2 screen_xy,screen_wh; // Where the rendering shows up on the viewport.
	slScalar yaw/*horizontal*/, pitch/*vertical*/, roll; /// Rotation in Degrees
	//bool screen_maintainaspect; // Similar behavior to slBox.maintainaspect
    s3dVec3 GetNormal (); // Calculates normal from yaw & pitch.
};
s3dCamera* s3dGetMainCamera ();

void s3dInit ();
void s3dQuit ();
void s3dRender (s3dCamera* cam = s3dGetMainCamera()); // Draws to the screen.
void s3dRender (slTargetTexRef target, s3dCamera* cam = s3dGetMainCamera()); // Draws to this texture.
void s3dSetDistanceFog (bool enabled, GLcolor3 rgb = 0, GLfloat multiplier = 0.25);
void s3dSetLightPos (bool phong_enabled, bool ortho_mode = true, GLvec4 light_xyz = GLvec4(0,-1,0)); // light_xyz.w is unused.
// ^ ortho_mode makes the light_xyz be treated directly as a light direction vector, instead of as a light position.
void s3dSetLightDir (bool is_directional, GLvec4 light_dir = GLvec4(0,-1,0), slScalar field_angle = 90); // light_dir.w is unused, field_angle is in degrees
// ^ this direction & angle have no effect if ortho_mode is on (ortho light is not directional... even though it's solely a direction).
void s3dSetEnvironmentAmbientColor (GLcolor3 color);
void s3dSetLightColor (GLcolor3 color);
void s3dSetLightStrength (GLfloat strength);
void s3dSetDiffuseVoxeling (bool voxeling_enabled, GLfloat voxel_size);
void s3dSetSpecularVoxeling (bool voxeling_enabled, GLfloat voxel_size);
void s3dSetFogVoxeling (bool voxeling_enabled, GLfloat voxel_size);

// Convenience macros for compatibility with original API style.
#define s3dSetProjectionMatrix(mat) (s3dGetMainCamera()->projection_matrix = (mat))
#define s3dGetCamPos() (s3dGetMainCamera()->origin)
#define s3dSetCamPos(vec) (s3dGetMainCamera()->origin = (vec))
inline slForceInline void s3dGetCamRotations (slScalar* horizontal, slScalar* vertical, slScalar* roll) // Safe to pass NULL for ones you don't want to retrieve.
{
	if (horizontal) *horizontal = s3dGetMainCamera()->yaw;
	if (vertical) *vertical = s3dGetMainCamera()->pitch;
	if (roll) *roll = s3dGetMainCamera()->roll;
};
inline slForceInline void s3dSetCamRotations (slScalar* horizontal, slScalar* vertical, slScalar* roll) // Safe to pass NULL for ones you don't want to provide.
{
	if (horizontal) s3dGetMainCamera()->yaw = *horizontal;
	if (vertical) s3dGetMainCamera()->pitch = *vertical;
	if (roll) s3dGetMainCamera()->roll = *roll;
};
inline slForceInline s3dVec3 s3dGetCamNormal ()
{
    return s3dGetMainCamera()->GetNormal();
}

struct s3dBumpInfo
{
    char* filepath;
    inline s3dBumpInfo (char* filepath) slForceInline
    {
        this->filepath = filepath;
    }
};
class s3dBumpTexture : public slTexture
{
    s3dBumpInfo bump_tree_key;
    slTreeNodeData bump_tree_data;
    friend class s3dBumpTexturesTreeProto;

    s3dBumpTexture* next_load;
    friend class s3dBumpAsyncQueueProto;

public:
    s3dBumpTexture (s3dBumpInfo search_key);
    ~s3dBumpTexture () override;
    void Unregister () override;
};
struct s3dBumpTexRef : public slTexRef
{
	s3dBumpTexRef (s3dBumpTexture* raw_ref) : slTexRef(raw_ref) {};
};
s3dBumpTexRef s3dLoadBumpMap (char* path);



#include <slice3d/slicephys3d.h>
