#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct objVertexPos
{
    float x, y, z, w;
};
struct objVertexTex
{
    float u, v, w;
};
struct objVertexNormal
{
    float x, y, z;
};
struct objVertexRef
{
    int pos_id, tex_id, normal_id;
};
struct objFace
{
     objVertexRef* vertex_ref_buf;
     int vertex_ref_count;
};
struct objColor
{
    float r, g, b;
};
struct objMaterial
{
    char* name; // freed after loading is complete, but during loading is guaranteed to exist.
    objColor
        ambient_color,
        diffuse_color,
        specular_color;
    float specular_exponent;
    char mag_filter_nearest;
    char ignore_environment_ambient;
    char* ambient_texpath; // will be NULL if not specified by the file
    char* diffuse_texpath; // will be NULL if not specified by the file
    char* specular_texpath; // will be NULL if not specified by the file
    char* opacity_texpath; // will be NULL if not specified by the file
    char* bump_texpath; // will be NULL if not specified by the file
};
extern objMaterial objFallbackMaterial;
struct objGroup
{
    union
    {
        objMaterial* material;
        char* material_name; // only used during loading, but may even be NULL then.
    };
    objFace* face_buf;
    int face_count;
};
struct objModel
{
    objVertexPos* vertex_pos_buf;
    int vertex_pos_count;
    objVertexNormal* vertex_normal_buf;
    int vertex_normal_count;
    objVertexTex* vertex_tex_buf;
    int vertex_tex_count;
    objMaterial* material_buf;
    int material_count;
    objGroup* group_buf;
    int group_count;
};

void objFree (objModel* model);
objModel* objLoad (char* basepath, char* filename);
