#include <slice.h>



slAsyncNonblockingQueue::slAsyncNonblockingQueue (int offsetof_next)
{
    this->offsetof_next = offsetof_next;
}
void slAsyncNonblockingQueue::Init ()
{
    sync = SDL_CreateMutex();
}
void slAsyncNonblockingQueue::Quit ()
{
    SDL_DestroyMutex(sync);
}

void slAsyncNonblockingQueue::Push (void* item)
{
    SDL_LockMutex(sync);

    *last_ptr = item;
    last_ptr = item + offsetof_next;
    *last_ptr = NULL;

    SDL_UnlockMutex(sync);
}
void* slAsyncNonblockingQueue::Get () // Will immediately return NULL if there is nothing.
{
    SDL_LockMutex(sync);

    void* out = first;
    if (first)
    {
        // We are pulling this item out of the queue.
        void** next_ptr = first + offsetof_next;
        first = *next_ptr;

        // If we pulled out the last item, we have to reset last_ptr.
        if (!first) last_ptr = &first;
    }

    SDL_UnlockMutex(sync);

    return out;
}



slAsyncBlockingQueue::slAsyncBlockingQueue (int offsetof_next) : nb_queue(offsetof_next) {};
void slAsyncBlockingQueue::Init ()
{
    nb_queue.Init();

    waiting = SDL_CreateSemaphore(0);
}
void slAsyncBlockingQueue::Quit ()
{
    SDL_DestroySemaphore(waiting);

    nb_queue.Quit();
}

void slAsyncBlockingQueue::Push (void* item)
{
    if (item) nb_queue.Push(item);

    SDL_SemPost(waiting);
}
void* slAsyncBlockingQueue::Get ()
{
    SDL_SemWait(waiting);

    return nb_queue.Get();
}



slConsumerQueue::slConsumerQueue (int offsetof_next,/* void (*item_func) (void*),*/ int consumer_count = 1) : queue(offsetof_next)
{
    this->consumer_count = consumer_count;
    //this->item_func = item_func;
    consumer_threads = malloc(sizeof(SDL_Thread*) * consumer_count);
}
slConsumerQueue::~slConsumerQueue ()
{
    free(consumer_threads);
}
void slConsumerQueue::Init ()
{
    /*
        We have to initialize the SDL mutex & semaphore outside of the
        constructor, because the slAsyncQueue is likely to be declared
        globally, which means the constructor will run *before* SDL_Init,
        meaning functions to create the mutex, semaphore, and threads could
        fail.
    */

    queue.Init();

    for (int i = 0; i < consumer_count; i++)
        consumer_threads[i] = SDL_CreateThread(Consume_Wrap,"slAsyncQueue consumer loop",this);
}
void slConsumerQueue::Quit ()
{
    /*
        Similar to why Init code is not part of constructor, if we put
        the Quit code in a destructor it will run after SQL_Quit already
        was called, if the slAsyncQueue was declared globally. Which means
        the threading functions wouldn't make any sense in that context, or
        could even cause a crash.
    */

    for (int i = 0; i < consumer_count; i++)
        queue.Push(NULL);
    for (int i = 0; i < consumer_count; i++)
        SDL_WaitThread(consumer_threads[i],NULL);

    queue.Quit();
}

void slConsumerQueue::Push (void* item)
{
    queue.Push(item);
}

void slConsumerQueue::PreLoop () {};
void slConsumerQueue::PostLoop () {};

void slConsumerQueue::Consume ()
{
    PreLoop();
    while (true)
    {
        void* item = queue.Get();
        if (!item) break;
        ItemFunc(item);
    }
    PostLoop();
}
static void slConsumerQueue::Consume_Wrap (slConsumerQueue* self)
{
    self->Consume();
}
