#include <slice.h>
using namespace sl_internal;

typedef slScalar slQuadScalar __attribute__((vector_size(sizeof(slScalar)*4)));
//typedef GLfloat slQuadGLfloat __attribute__((vector_size(sizeof(GLfloat)*4)));
extern slScalar slWindowAspect,slInvWindowAspect;
extern GLfloat slWindowAspect_Narrow,slInvWindowAspect_Narrow;
slCorners slGetBoxCorners (slBox* box)
{
	slScalar x = box->xy.x;
	slScalar y = box->xy.y;
	slScalar w = box->wh.w;
	slScalar h = box->wh.h;
	if (box->maintainaspect)
	{
		if (slWindowAspect > 1)
		{
			w *= slInvWindowAspect;

			x = ((x - 0.5f) * slInvWindowAspect) + 0.5f;
		}
		else
		{
			h *= slWindowAspect;
			y = ((y - 0.5f) * slWindowAspect) + 0.5f;
		};
	};
	slQuadScalar pxes = {x,x,x,x};
	pxes += (slQuadScalar){0,w,0,w};
	slQuadScalar pyes = {y,y,y,y};
	pyes += (slQuadScalar){0,0,h,h};
	if (unlikely(box->rot))
	{
		slScalar sinvalue = slsin(slDegToRad(box->rotangle));
		slScalar cosvalue = slpow(1 - sinvalue * sinvalue,0.5);
		bool rotoffset = !box->rotcenter;
		slScalar rotpointx,rotpointy;
		if (unlikely(rotoffset))
		{
			rotpointx = box->rotpoint.x;
			rotpointy = box->rotpoint.y;
			/// No idea whether or not this is needed...
			/*if (box->maintainaspect)
			{
				// Scale rotation point.
				if (slWindowAspect > 1) rotpointx = ((rotpointx - 0.5) * slInvWindowAspect) + 0.5;
				else rotpointy = ((rotpointy - 0.5) * slWindowAspect) + 0.5;
			};*/
			/// ...should do some testing to figure that out!
			pxes -= rotpointx;
			pyes -= rotpointy;
		};
		//pxes *= slWindowAspect;
		pyes *= slInvWindowAspect;//
		slQuadScalar pxes_ = pxes;
		slQuadScalar pyes_ = pyes;
		pxes = pxes_ * cosvalue - pyes_ * sinvalue;
		pyes = pyes_ * cosvalue + pxes_ * sinvalue;
		//pxes *= slInvWindowAspect;
		pyes *= slWindowAspect;//
		if (unlikely(rotoffset))
		{
			pxes += rotpointx;
			pyes += rotpointy;
		};
	};
	return {pxes[0],pyes[0],pxes[1],pyes[1],pxes[2],pyes[2],pxes[3],pyes[3]};
};
slCorners slGetBoxTexCorners (slBox* box, slTexture* texref)
{
	slScalar x = box->xy.x;
	slScalar y = box->xy.y;
	slScalar w = box->wh.w;
	slScalar h = box->wh.h;
	slScalar aratio = (w / h) / texref->aspect;
	if (!box->maintainaspect) aratio *= slWindowAspect;
	slScalar prev_dim;
	if (aratio > 1)
	{
		prev_dim = w;
		w /= aratio;
		x += (prev_dim - w) * 0.5f;
	}
	else
	{
		prev_dim = h;
		h *= aratio;
		y += (prev_dim - h) * 0.5f;
	};

	// From slGetBoxCorners...
	if (box->maintainaspect)
	{
		if (slWindowAspect > 1)
		{
			w *= slInvWindowAspect;
			x = ((x - 0.5f) * slInvWindowAspect) + 0.5f;
		}
		else
		{
			h *= slWindowAspect;
			y = ((y - 0.5f) * slWindowAspect) + 0.5f;
		};
	};
	slQuadScalar pxes = {x,x,x,x};
	pxes += (slQuadScalar){0,w,0,w};
	slQuadScalar pyes = {y,y,y,y};
	pyes += (slQuadScalar){0,0,h,h};
	if (unlikely(box->rot))
	{
		slScalar sinvalue = slsin(slDegToRad(box->rotangle));
		slScalar cosvalue = slpow(1 - sinvalue * sinvalue,0.5);
		bool rotoffset = !box->rotcenter;
		slScalar rotpointx,rotpointy;
		if (unlikely(rotoffset))
		{
			rotpointx = box->rotpoint.x;
			rotpointy = box->rotpoint.y;
			pxes -= rotpointx;
			pyes -= rotpointy;
		};
		//pxes *= slWindowAspect;
		pyes *= slInvWindowAspect;//
		slQuadScalar pxes_ = pxes;
		slQuadScalar pyes_ = pyes;
		pxes = pxes_ * cosvalue - pyes_ * sinvalue;
		pyes = pyes_ * cosvalue + pxes_ * sinvalue;
		//pxes *= slInvWindowAspect;
		pyes *= slWindowAspect;//
		if (unlikely(rotoffset))
		{
			pxes += rotpointx;
			pyes += rotpointy;
		};
	};
	return {pxes[0],pyes[0],pxes[1],pyes[1],pxes[2],pyes[2],pxes[3],pyes[3]};
};
slList slBoxes("Slice: Boxes",offsetof(slBox,_index_),false);
slBlockAllocator slBoxAllocator ("Slice: Box Allocator",sizeof(slBox),384);
slBox* slCreateBox ()
{
	//slBox* out = malloc(sizeof(slBox));
	slBox* out = slBoxAllocator.Allocate();

	out->texref_swapchain = slInitTexSwapChain();
	out->hovertexref_swapchain = slInitTexSwapChain();
	out->rot = false;
	out->visible = true;
	out->onclick = NULL;
	out->onrightclick = NULL;
	out->backcolor = slBlankColor;
	out->bordercolor = slBlankColor;
	out->hoverbackcolor = slBlankColor;
	out->hoverbordercolor = slBlankColor;
	out->texbordercolor = slBlankColor;
	out->hovertexbordercolor = slBlankColor;

	out->hoverable = false;
	out->drawmask = {255,255,255,255};
	out->hoverdrawmask = {255,255,255,255};
	out->tex_align_x = slAlignCenter;
	out->tex_align_y = slAlignCenter;
	out->tex_wh = 1;
	out->maintainaspect = false;
	out->aspect_align_x = slAlignCenter;
	out->aspect_align_y = slAlignCenter;
	out->border_thickness = 1;
	out->scissor = NULL;

	slBoxes.Add(out);
	return out;
};
slBox* slCreateBox (slTexRef tex)
{
    slBox* out = slCreateBox();
    out->SetTexRef(tex);
    return out;
}
slBox* slCreateBox (slTexRef tex, slTexRef hovertex)
{
    slBox* out = slCreateBox(tex);
    out->SetHoverTexRef(hovertex);
    return out;
}
void slDestroyBox (slBox* todel)
{
	slBoxes.Remove(todel);

	slQuitTexSwapChain(&todel->texref_swapchain);
	slQuitTexSwapChain(&todel->hovertexref_swapchain);

	//free(todel);
	slBoxAllocator.Release(todel);
};
bool slPointOnBox (slBox* box, slVec2 point)
{
	slCorners corners;
	if (box->rot)
	{
		slBox clone = *box;
		slScalar angle = -slDegToRad(clone.rotangle);
		slScalar angcos = slcos(angle);
		slScalar angsin = slsin(angle);
		slVec2 center = clone.xy + clone.wh * 0.5f;
		if (clone.maintainaspect)
		{
			// Scale center point.
			if (slWindowAspect > 1) center.x = ((center.x - 0.5f) * slInvWindowAspect) + 0.5f;
			else center.y = ((center.y - 0.5f) * slWindowAspect) + 0.5f;
		};

		slVec2 offset;
		if (!clone.rotcenter) offset = clone.rotpoint - center;
		else offset = 0;

		point -= center;
		point -= offset;

		point.x *= slWindowAspect;

		slVec2 _point = point;
		point.x = point.x * angcos - point.y * angsin;
		point.y = _point.y * angcos + _point.y * angsin;

		point.x /= slWindowAspect;

		point += offset;
		point += center;

		clone.rot = false;
		//clone.maintainaspect = false;
		corners = slGetBoxCorners(&clone);
	}
	else corners = slGetBoxCorners(box);
	return point.x > corners.p00x && point.x < corners.p01x && point.y < corners.p10y && point.y > corners.p00y;
	/*

		Corners as seen on-screen:

		10	11
		00	01

	*/
};
void slBox::PutWithin (slBox* outer)
{
	// To do: take rotation into account.
	xy *= outer->wh;
	wh *= outer->wh;
	xy += outer->xy;
};
bool slBoxesInOrder (slBox* box1, slBox* box2)
{
	return box2->z <= box1->z;
};


namespace sl_internal
{

bool slOnClickUI (slVec2 cursor, bool leftclick)
{
	slSort(&slBoxes,slBoxesInOrder);
	slBU pos = slBoxes.itemcount;
	slBU func_offset = leftclick ? offsetof(slBox,onclick) : offsetof(slBox,onrightclick);
	while (pos)
	{
		slBox* box = slBoxes.items[--pos];
		void (*func) (slBox*) = *(void (**) (slBox*))((void*)box + func_offset);
		if (func && box->visible)
		{
			if (slPointOnBox(box,cursor))
			{
				func(box);
				return true;
			};
		};
	};
	return false;
};

};

slBox* slGetHoveredBox (slVec2 cursor)
{
	slSort(&slBoxes,slBoxesInOrder);
	for (slBU cur = slBoxes.itemcount; cur; )
	{
		slBox* box = slBoxes.items[--cur];
		if (box->visible && box->hoverable)
			if (slPointOnBox(box,cursor))
				return box;
	};
	return NULL;
};
void slBoxHoverAbsorb (slBox* box)
{
	box->hoverable = true;
	box->hoverbordercolor = box->bordercolor;
	box->hoverbackcolor = box->backcolor;
	box->hoverdrawmask = box->drawmask;
	box->hovertexbordercolor = box->texbordercolor;
	slQueueTexSwap(&box->hovertexref_swapchain,NULL);
};
void slAspectCorrection (GLxywh* xywh, Uint8 aspect_align_x, Uint8 aspect_align_y)
{
	if (slWindowAspect_Narrow > 1)
	{
		xywh->w *= slInvWindowAspect_Narrow;
		float anchor;
		switch (aspect_align_x)
		{
			case slAlignCenter: anchor = 0.5; break;
			case slAlignLeft: anchor = 0; break;
			case slAlignRight: anchor = 1;
		};
		xywh->x = ((xywh->x - anchor) * slInvWindowAspect_Narrow) + anchor;
	}
	else
	{
		xywh->h *= slWindowAspect_Narrow;
		float anchor;
		switch (aspect_align_y)
		{
			case slAlignCenter: anchor = 0.5; break;
			case slAlignTop: anchor = 0; break;
			case slAlignBottom: anchor = 1;
		};
		xywh->y = ((xywh->y - anchor) * slWindowAspect_Narrow) + anchor;
	};
};
inline GLxywh slCalcBoxXYWH (GLvec2 pos, GLvec2 dims, bool maintainaspect, Uint8 aspect_align_x, Uint8 aspect_align_y)
{
	GLfloat x = pos.x;
	GLfloat y = pos.y;
	GLfloat w = dims.w;
	GLfloat h = dims.h;
	GLxywh xywh = {x,y,w,h};
	if (maintainaspect) slAspectCorrection(&xywh,aspect_align_x,aspect_align_y);
	return xywh;
};
inline GLxywh slCalcTexXYWH (GLvec2 pos, GLvec2 dims, bool maintainaspect, GLfloat tex_aspect, GLvec2 tex_wh, Uint8 tex_align_x, Uint8 tex_align_y, Uint8 aspect_align_x, Uint8 aspect_align_y)
{
	GLfloat start_w = dims.w;
	GLfloat start_h = dims.h;
	/**
		Rel: w=0.9,h=0.9,x=0.05,y=0.05
		Rel: w=tex_w,h=tex_h,x=(1-tex_w)*.5,h=(1-tex_h)*.5
	**/
	pos += dims * ((GLvec2(1) - tex_wh) * 0.5f);
	dims *= tex_wh;
	GLfloat x = pos.x;
	GLfloat y = pos.y;
	GLfloat w = dims.w;
	GLfloat h = dims.h;
	/*
	GLfloat tex_w = box->tex_w;
	GLfloat tex_h = box->tex_h;
	x *= tex_w;
	y *= tex_h;
		x += (1 - tex_w) * 0.5f;
		y += (1 - tex_h) * 0.5f;
	w *= tex_w;
	h *= tex_h;
	*/



	GLfloat aratio = (w / h) / tex_aspect;
	if (!maintainaspect) aratio *= slWindowAspect_Narrow;
	if (aratio > 1)
	{
		GLfloat prev_w = w;
		w /= aratio;
		x += (prev_w - w) * 0.5f;
	}
	else
	{
		GLfloat prev_h = h;
		h *= aratio;
		y += (prev_h - h) * 0.5f;
	};



	if (tex_align_x != slAlignCenter)
	{
		GLfloat xoff = (start_w - w) * 0.5f;
		if (tex_align_x == slAlignLeft) x -= xoff;
		else x += xoff;
	};
	if (tex_align_y != slAlignCenter)
	{
		GLfloat yoff = (start_h - h) * 0.5f;
		if (tex_align_y == slAlignTop) y -= yoff;
		else y += yoff;
	};

	GLxywh xywh = {x,y,w,h};
	if (maintainaspect) slAspectCorrection(&xywh,aspect_align_x,aspect_align_y);
	return xywh;
};
void slRenderPrepare (slBox* box, slBox* hoverbox)
{
	slStepTexSwapChain(&box->texref_swapchain);
	slStepTexSwapChain(&box->hovertexref_swapchain);



	if (box->visible ? false : true)
	{
		box->render.draw_anything = false;
		return;
	};

	slTexture* texref;
	SDL_Color backcolor,bordercolor,gapcolor,texbordercolor,drawmask;
	if (unlikely(box == hoverbox))
	{
		if (box->hovertexref_swapchain.texref) texref = box->hovertexref_swapchain.texref;
		else texref = box->texref_swapchain.texref;
		backcolor = box->hoverbackcolor;
		bordercolor = box->hoverbordercolor;
		texbordercolor = box->hovertexbordercolor;
		drawmask = box->hoverdrawmask;
	}
	else
	{
		texref = box->texref_swapchain.texref;
		backcolor = box->backcolor;
		bordercolor = box->bordercolor;
		texbordercolor = box->texbordercolor;
		drawmask = box->drawmask;
	};

	bool tex_ready;
	if (texref) tex_ready = texref->ready;
	else tex_ready = false;

	box->render.draw_back = backcolor.a;
	box->render.draw_texture = tex_ready && drawmask.a;
	box->render.draw_texborder = tex_ready && texbordercolor.a;
	box->render.draw_border = bordercolor.a;

	box->render.draw_anything = box->render.draw_back || box->render.draw_texture || box->render.draw_texborder || box->render.draw_border;
	if (!box->render.draw_anything) return;

	box->render.backcolor = backcolor;
	box->render.bordercolor = bordercolor;
	box->render.texbordercolor = texbordercolor;
	box->render.drawmask = drawmask;

	box->render.color_setup_texborder = !box->render.draw_back;
	box->render.color_setup_boxborder = !(box->render.draw_back || box->render.draw_texborder);

	GLvec2 pos(box->xy);
	GLvec2 dims(box->wh);
	GLfloat cosvalue,sinvalue;
	GLvec2 rotpoint;
	if (box->rot)
	{
		sinvalue = slDegToRad_F((GLfloat)box->rotangle);
		cosvalue = cosf(sinvalue);
		sinvalue = sinf(sinvalue);
		if (!box->rotcenter)
		{
			rotpoint = GLvec2(box->rotpoint);
			goto OFFCENTER;
		};
	}
	else
	{
		sinvalue = 0;
		cosvalue = 1;
	};
	rotpoint = pos + (dims * 0.5f);
	OFFCENTER:

	if (box->maintainaspect)
	{
		// Scale rotation point.
		GLxywh rotpoint_xywh = {rotpoint.x,rotpoint.y,0,0}; // w and h don't matter
		slAspectCorrection(&rotpoint_xywh,box->aspect_align_x,box->aspect_align_y);
		rotpoint.x = rotpoint_xywh.x; rotpoint.y = rotpoint_xywh.y;
	};

	box->render.rotpoint = rotpoint;
	box->render.sinvalue = sinvalue;
	box->render.cosvalue = cosvalue;

	box->render.texref = texref;

	if (box->render.draw_back || box->render.draw_border)
		box->render.box_xywh = slCalcBoxXYWH(pos,dims,box->maintainaspect,box->aspect_align_x,box->aspect_align_y);
	if (box->render.draw_texture || box->render.draw_texborder)
		box->render.tex_xywh = slCalcTexXYWH(pos,dims,box->maintainaspect,texref->aspect,box->tex_wh,box->tex_align_x,box->tex_align_y,box->aspect_align_x,box->aspect_align_y);
};





slList slZHooks("Slice: Z-Hooks",slNoIndex,false);
void slAddZHook (slZHook* zhook)
{
	slZHooks.Add(zhook);
};
void slRemoveZHook (slZHook* zhook)
{
	slZHooks.Remove(zhook);
};
bool slZHooksInOrder (slZHook* zhook1, slZHook* zhook2)
{
	return zhook2->z <= zhook1->z;
};

SDL_Rect slBox::CalcScreenPos ()
{
	slCorners corners = slGetBoxCorners(this);
	int screen_w,screen_h;
	SDL_GetWindowSize(slGetWindow(),&screen_w,&screen_h);
	SDL_Rect out;
	out.x = corners.p00x * screen_w;
	out.y = (1 - corners.p10y) * screen_h;
	out.w = (corners.p01x - corners.p00x) * screen_w;
	out.h = (corners.p10y - corners.p00y) * screen_h;
	return out;
};



namespace sl_internal
{


void slRenderPrepareAll (slBox* hoverbox)
{
	slDoWork(&slBoxes,slRenderPrepare,hoverbox);
	slSort(&slZHooks,slZHooksInOrder);
	// Boxes should already be sorted by this point since slGetHoveredBox is always called every frame.
};

}

void slDrawUI ()
{
	glDisable(GL_DEPTH_TEST);

	// Set WindowAspect for Rotate Programs
	glUseProgram(slColorRotateProgram.program);
	glUniform1f(slColorRotateProgram_WindowAspect,slWindowAspect);
	glUseProgram(slTexRotateProgram.program);
	glUniform1f(slTexRotateProgram_WindowAspect,slWindowAspect);

	//glBindBuffer(GL_ARRAY_BUFFER,slBoxVertsBuf);
	slBindFullscreenArray();
	slTexture* prevtex = NULL;



	slBU zhook_id = 0;
	slZHook* zhook;
	if (slZHooks.itemcount) zhook = *slZHooks.items;
	else zhook = NULL;

	GLuint CurrentProgram = 0;

	for (slBU cur = 0; cur < slBoxes.itemcount; cur++)
	{
		slBox* box = *(slBoxes.items + cur);

		if (zhook)
		{
			glDisable(GL_SCISSOR_TEST);

			if (box->z < zhook->z)
			{
				DO_ZHOOK:
				zhook->func(zhook->userdata);
				if (++zhook_id < slZHooks.itemcount)
				{
					zhook = *(slZHooks.items + zhook_id);
					if (box->z < zhook->z) goto DO_ZHOOK;
				}
				else zhook = NULL;

				slBindFullscreenArray(); // In case this has been clobbered.
				CurrentProgram = 0;
				prevtex = NULL;
			};
		};

		if (box->render.draw_anything)
		{
			if (box->scissor)
			{
				glEnable(GL_SCISSOR_TEST);
				glScissor(box->scissor->x,box->scissor->y,box->scissor->w,box->scissor->h);
			}
			else glDisable(GL_SCISSOR_TEST);
			if (box->render.draw_back)
			{
				if (CurrentProgram != slColorRotateProgram.program) glUseProgram(CurrentProgram = slColorRotateProgram.program);
				glUniform2fv(slColorRotateProgram_RotPoint,1,(GLfloat*)&box->render.rotpoint);
				glUniform1f(slColorRotateProgram_SinValue,box->render.sinvalue);
				glUniform1f(slColorRotateProgram_CosValue,box->render.cosvalue);

				glUniform4fv(slColorRotateProgram_XYWH,1,(GLfloat*)&box->render.box_xywh);
				glUniform4fv(slColorRotateProgram_Color,1,(GLfloat*)&box->render.backcolor);//slUniformFromColor(uniform_color,backcolor);
				glDrawArrays(GL_TRIANGLE_FAN,0,4);
			};
			if (box->render.draw_texture)
			{
				if (CurrentProgram != slTexRotateProgram.program) glUseProgram(CurrentProgram = slTexRotateProgram.program);
				glUniform2fv(slTexRotateProgram_RotPoint,1,(GLfloat*)&box->render.rotpoint);
				glUniform1f(slTexRotateProgram_SinValue,box->render.sinvalue);
				glUniform1f(slTexRotateProgram_CosValue,box->render.cosvalue);

				if (box->render.texref != prevtex)
				{
					slSetDrawTexture(prevtex = box->render.texref);
					slSetTextureClamping(GL_CLAMP_TO_EDGE);
				};
				glUniform4fv(slTexRotateProgram_XYWH,1,(GLfloat*)&box->render.tex_xywh);
				glUniform4fv(slTexRotateProgram_Mask,1,(GLfloat*)&box->render.drawmask);//slUniformFromColor(uniform_drawmask,drawmask);
				glDrawArrays(GL_TRIANGLE_FAN,0,4);
			};
			if (box->render.draw_texborder)
			{
				glLineWidth(box->border_thickness);
				if (CurrentProgram != slColorRotateProgram.program) glUseProgram(CurrentProgram = slColorRotateProgram.program);
				if (box->render.color_setup_texborder)
				{
					glUniform2fv(slColorRotateProgram_RotPoint,1,(GLfloat*)&box->render.rotpoint);
					glUniform1f(slColorRotateProgram_SinValue,box->render.sinvalue);
					glUniform1f(slColorRotateProgram_CosValue,box->render.cosvalue);
				};
				glUniform4fv(slColorRotateProgram_XYWH,1,(GLfloat*)&box->render.tex_xywh);
				glUniform4fv(slColorRotateProgram_Color,1,(GLfloat*)&box->render.texbordercolor);//slUniformFromColor(uniform_color,texbordercolor);
				glDrawArrays(GL_LINE_LOOP,0,4);
				if (box->border_thickness > 1)
				{
					glPointSize(box->border_thickness);
					glDrawArrays(GL_POINTS,0,4);
				};
			};
			if (box->render.draw_border)
			{
				glLineWidth(box->border_thickness);
				if (CurrentProgram != slColorRotateProgram.program) glUseProgram(CurrentProgram = slColorRotateProgram.program);
				if (box->render.color_setup_boxborder)
				{
					glUniform2fv(slColorRotateProgram_RotPoint,1,(GLfloat*)&box->render.rotpoint);
					glUniform1f(slColorRotateProgram_SinValue,box->render.sinvalue);
					glUniform1f(slColorRotateProgram_CosValue,box->render.cosvalue);
				};
				glUniform4fv(slColorRotateProgram_XYWH,1,(GLfloat*)&box->render.box_xywh);
				glUniform4fv(slColorRotateProgram_Color,1,(GLfloat*)&box->render.bordercolor);//slUniformFromColor(uniform_color,bordercolor);
				glDrawArrays(GL_LINE_LOOP,0,4);
				if (box->border_thickness > 1)
				{
					glPointSize(box->border_thickness);
					glDrawArrays(GL_POINTS,0,4);
				};
			};
		};
	};

	glDisable(GL_SCISSOR_TEST);

	while (zhook_id < slZHooks.itemcount)
	{
		zhook = *(slZHooks.items + zhook_id++);
		zhook->func(zhook->userdata);
		slBindFullscreenArray(); // In case this has been clobbered.
	};
};


namespace sl_internal
{

void slInitUI ()
{
	slInitBoxRenderPrograms();
	slBoxes.HookAutoShrink();
};
void slQuitUI ()
{
	slBoxes.UntilEmpty(slDestroyBox);
	slBoxAllocator.FreeAllBlocks();
	slZHooks.Clear(NULL);
	slQuitBoxRenderPrograms();
};

};
