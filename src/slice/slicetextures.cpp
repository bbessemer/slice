#include <slice.h>
slList slFonts("Slice: Fonts",slNoIndex,false); // Autoshrink off and no GC action, because items are never removed until engine exit.
slFont* slLoadFont (char* path)
{
	slFont* out;
	for (slBU cur = 0; cur < slFonts.itemcount; cur++)
	{
		out = *(slFonts.items + cur);
		if (slStrEq(path,out->path)) return out;
	};
	TTF_Font* font = TTF_OpenFont(path,256);//64);
	if (!font) return NULL;
	out = malloc(sizeof(slFont));
	out->font = font;
	out->path = slStrClone(path);
	slFonts.Add(out);
	return out;
};
void slDeallocFont (slFont* item)
{
	if (item->path) free(item->path);
	TTF_CloseFont(item->font);
};
char* slDefaultFontPath = NULL;
void slSetDefaultFontPath (char* path)
{
	if (slDefaultFontPath) free(slDefaultFontPath);
	slDefaultFontPath = slStrClone(path);
};
char* slGetDefaultFontPath ()
{
	return slDefaultFontPath;
};


SDL_Surface* slInvalidTextureSurf;
SDL_Surface* slEmptyTextureSurf;
#define slInvalidTexW 16
#define slInvalidTexBorder 1
void slCreateInvalidTexture ()
{
	slInvalidTextureSurf = SDL_CreateRGBSurfaceWithFormat(0,slInvalidTexW,slInvalidTexW,32,SDL_PIXELFORMAT_ABGR8888);
	SDL_Color* pixel = slInvalidTextureSurf->pixels;
	SDL_Color border_even = {255,255,255,255};
	SDL_Color border_odd = {0,0,0,255};
	SDL_Color inner_even {255,255,0,255};
	SDL_Color inner_odd {0,0,255,255};
	Uint8 border_low = slInvalidTexBorder;
	Uint8 border_high = slInvalidTexW - slInvalidTexBorder;
	for (Uint8 y = 0; y < slInvalidTexW; y++)
	{
		for (Uint8 x = 0; x < slInvalidTexW; x++)
		{
			bool border = x < border_low || x >= border_high || y < border_low || y >= border_high;
			bool odd = (x + y) & 1;
			*pixel++ = border ? (odd ? border_odd : border_even) : (odd ? inner_odd : inner_even);
		};
	};

	slEmptyTextureSurf = SDL_CreateRGBSurfaceWithFormat(0,1,1,32,SDL_PIXELFORMAT_ABGR8888);
	pixel = slEmptyTextureSurf->pixels;
	*pixel = {0,0,0,0};
};
void slFreeInvalidTexture ()
{
	SDL_FreeSurface(slInvalidTextureSurf);
	SDL_FreeSurface(slEmptyTextureSurf);
};

//slList slTextures("Slice: Textures",offsetof(slTexture,_index_),false);
slList slTexture::Textures = slList("Slice: Textures",offsetof(slTexture,_index_),false);
SDL_mutex* slTexListMutex;
SDL_atomic_t slTexturesQueuedForGC = {0};

extern void slSetTextureClamping (GLint mode);
SDL_Surface* slSurfaceFromTexture (GLuint tex)
{
	glBindTexture(GL_TEXTURE_2D,tex);
	GLint tex_w,tex_h;
	glGetTexLevelParameteriv(GL_TEXTURE_2D,0,GL_TEXTURE_WIDTH,&tex_w);
	glGetTexLevelParameteriv(GL_TEXTURE_2D,0,GL_TEXTURE_HEIGHT,&tex_h);
	SDL_Surface* surf = SDL_CreateRGBSurface(0,tex_w,tex_h,32,0x000000FF,0x0000FF00,0x00FF0000,0xFF000000);
	glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA,GL_UNSIGNED_INT_8_8_8_8_REV,surf->pixels);
	return surf;
};

struct slTexUploadTask
{
    slTexture* tex;
    SDL_Surface* upload_surf;
    inline slTexUploadTask (slTexture* tex, SDL_Surface* upload_surf) slForceInline
    {
        this->tex = tex;
        this->upload_surf = upload_surf;
    }
};
void slFlipSurfaceVertical (SDL_Surface* surf)
{
    int row_size = surf->pitch;
    void* temp_row = malloc(row_size);
    int half_height = surf->h >> 1;
    int last_row = surf->h - 1;
    for (int y = 0; y < half_height; y++)
    {
        void* row_a = surf->pixels + row_size * y;
        void* row_b = surf->pixels + row_size * (last_row - y);
        memcpy(temp_row,row_a,row_size);
        memcpy(row_a,row_b,row_size);
        memcpy(row_b,temp_row,row_size);
    }
    free(temp_row);
}
void slDoTexUpload (slTexUploadTask* finalization)
{
    slTexture* out = finalization->tex;
    SDL_Surface* surf = finalization->upload_surf;

    slFlipSurfaceVertical(surf);

	GLuint tex;
	glGenTextures(1,&tex);

	if (tex)
	{
		glBindTexture(GL_TEXTURE_2D,tex);
		glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA8,surf->w,surf->h,0,GL_RGBA,GL_UNSIGNED_BYTE,surf->pixels);
		//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		//slSetTextureClamping(GL_CLAMP_TO_EDGE);
        glGenerateMipmap(GL_TEXTURE_2D);
	}

	if (surf != slInvalidTextureSurf && surf != slEmptyTextureSurf)
		SDL_FreeSurface(surf);

    glFinish();

	out->tex = tex;
	out->ready = true;
    out->Abandon(); // At start of loading we called Reserve to avoid GC.

    delete finalization;
};
SDL_atomic_t slTexturesInFinalization = {0};
void slQueueTexUpload (slTexture* tex, SDL_Surface* surf)
{
    slBackgroundUploader.Push(
        slDoTexUpload,
        new slTexUploadTask(tex,surf),
        &slTexturesInFinalization
    );
}

SDL_mutex* slMutex_IMG_Load;
SDL_Surface* slLoadTexture_Surf (char* path)
{
	if (!path) return NULL;
	SDL_LockMutex(slMutex_IMG_Load);
	SDL_Surface* surf = IMG_Load(path);
	SDL_UnlockMutex(slMutex_IMG_Load);
    if (!surf) printf("Failed to load texture from '%s'\n",path);
	return surf;
};
SDL_mutex* slMutex_libfreetype;
SDL_Surface* slRenderText_Surf (char* str, SDL_Color color, slFont* font)
{
	if (!str) return NULL;
	SDL_LockMutex(slMutex_libfreetype);
	SDL_Surface* surf = TTF_RenderUTF8_Blended(font->font,str,color);
	SDL_UnlockMutex(slMutex_libfreetype);
	return surf;
};
SDL_Surface* slConvertSurface_RGBA32 (SDL_Surface* original)
{
	SDL_Surface* converted = SDL_ConvertSurfaceFormat(original,SDL_PIXELFORMAT_ABGR8888,0);
	return converted;
};
SDL_Surface* slConvertSurface_RGBA32_Free (SDL_Surface* original)
{
	if (!original) return NULL;
	SDL_Surface* out = slConvertSurface_RGBA32(original);
	SDL_FreeSurface(original);
	return out;
};

struct slImageTexturesTreeProto : slTreeMap<slImageInfo,slImageTexture>
{
    slImageTexturesTreeProto ()
        : slTreeMap(&slImageTexture::image_tree_key,&slImageTexture::image_tree_data)
    {}
    int Compare (slImageInfo* info_a, slImageInfo* info_b) override
    {
        /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */

        return strcmp(info_a->loaded_from,info_b->loaded_from);
    }
    void PrintKey (slImageInfo* info) override
    {
        printf("image '%s'\n",info->loaded_from);
    }
}
slImageTexturesTree;

struct slTextTexturesTreeProto : slTreeMap<slTextInfo,slTextTexture>
{
    slTextTexturesTreeProto ()
        : slTreeMap(&slTextTexture::text_tree_key,&slTextTexture::text_tree_data)
    {}
    int Compare (slTextInfo* info_a, slTextInfo* info_b) override
    {
        /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */

        int diff = strcmp(info_a->text,info_b->text);
        if (diff) return diff;

        diff = *(int*)&info_b->text_color - *(int*)&info_a->text_color;
        if (diff) return diff;

        diff = *(int*)&info_b->back_color - *(int*)&info_a->back_color;
        if (diff) return diff;

        return info_b->font - info_a->font;
    }
    void PrintKey (slTextInfo* info) override
    {
        printf(
            "text '%s' #%.2X%.2X%.2X%,2X #%.2X%.2X%.2X%,2X 0x%llX\n",
            info->text,
            info->text_color.r,info->text_color.g,info->text_color.b,info->text_color.a,
            info->back_color.r,info->back_color.g,info->back_color.b,info->back_color.a,
            info->font
        );
    }
}
slTextTexturesTree;

class slTextAsyncQueueProto : public slConsumerQueue
{
public:
    slTextAsyncQueueProto () : slConsumerQueue(offsetof(slTextTexture,next_load)) {};
private:
    void ItemFunc (void* queue_item) override
    {
        slTextTexture* out = queue_item;

    	//printf("text texture is loading\n");
    	// SDL_Delay(500); // Simulate very slow text rendering.

    	if (!out->text_tree_key.font) goto TEXT_PROBLEM;
    	if (!*out->text_tree_key.text) goto NO_TEXT;

    {
    	SDL_Surface* original = slRenderText_Surf(
            out->text_tree_key.text,
            out->text_tree_key.text_color,
            out->text_tree_key.font
        );
    	if (original)
    	{
    		SDL_Surface* converted = slConvertSurface_RGBA32(original);
    		SDL_FreeSurface(original);
    		if (converted)
    		{
    			out->aspect = converted->w / (slScalar)converted->h;
    			out->dims_valid = true;
                slQueueTexUpload(out,converted);
    			return;
    		};
    	};
    }

    	TEXT_PROBLEM:

    	// Failure
    	//printf("failed to render text\n");
    	out->aspect = 1;
    	out->dims_valid = true;
        slQueueTexUpload(out,slInvalidTextureSurf);
    	return;

    	NO_TEXT:
    	out->aspect = 1;
    	out->dims_valid = true;
    	out->ready = true;
        out->Abandon(); // At start of loading we called Reserve to avoid GC.
    }
}
slTextAsyncQueue;
class slImageAsyncQueueProto : public slConsumerQueue
{
public:
    slImageAsyncQueueProto () : slConsumerQueue(offsetof(slImageTexture,next_load)) {};
private:
    void ItemFunc (void* queue_item) override
    {
        slImageTexture* out = queue_item;

    	//printf("image texture is loading\n");
    	//SDL_Delay(500); // Simulate very slow disk.

    	SDL_Surface* original = slLoadTexture_Surf(out->image_tree_key.loaded_from);
    	if (original)
    	{
    		SDL_Surface* converted = slConvertSurface_RGBA32(original);
    		SDL_FreeSurface(original);
    		if (converted)
    		{
    			out->aspect = converted->w / (slScalar)converted->h;
    			out->dims_valid = true;
                slQueueTexUpload(out,converted);
    			return;
    		};
    	};

    	// Failure
    	//printf("image texture FAILED\n");
    	out->aspect = 1;
    	out->dims_valid = true;
        slQueueTexUpload(
            out,
            out->image_tree_key.loaded_from ? slInvalidTextureSurf : slEmptyTextureSurf
        );
    }
}
slImageAsyncQueue;

slTexture::slTexture ()
{
    slTextures.Add(this);
}
slTexture::~slTexture () {}
void slTexture::Unregister ()
{
    slTextures.Remove(this);
}

slTextTexture::slTextTexture (slTextInfo search_key) : text_tree_key(search_key)
{
    text_tree_key.text = slStrAlwaysClone(text_tree_key.text);
    slTextTexturesTree.Insert(this);
    //printf("count:   %d\n",slTextTexturesTree.total_items);
    //printf("recount: %d\n",slTextTexturesTree.Recount());
    //slTextTexturesTree.PrintInOrder();
}
slTextTexture::~slTextTexture ()
{
    free(text_tree_key.text);
}
void slTextTexture::Unregister ()
{
    slTexture::Unregister();
    slTextTexturesTree.Remove(this);
    //printf("count:   %d\n",slTextTexturesTree.total_items);
    //printf("recount: %d\n",slTextTexturesTree.Recount());
    //slTextTexturesTree.PrintInOrder();
}

slImageTexture::slImageTexture (slImageInfo search_key) : image_tree_key(search_key)
{
    image_tree_key.loaded_from = slStrAlwaysClone(image_tree_key.loaded_from);
    slImageTexturesTree.Insert(this);
    //printf("count:   %d\n",slImageTexturesTree.total_items);
    //printf("recount: %d\n",slImageTexturesTree.Recount());
    //slImageTexturesTree.PrintInOrder();
}
slImageTexture::~slImageTexture ()
{
    free(image_tree_key.loaded_from);
}
void slImageTexture::Unregister ()
{
    slTexture::Unregister();
    slImageTexturesTree.Remove(this);
    //printf("count:   %d\n",slImageTexturesTree.total_items);
    //printf("recount: %d\n",slImageTexturesTree.Recount());
    //slImageTexturesTree.PrintInOrder();
}

static slImageTexRef slImageTexture::Load (char* path)
{
	if (!path) path = "";

    SDL_LockMutex(slTexListMutex);

    slImageInfo search_key(path);
    slImageTexture* found_tex = slImageTexturesTree.Lookup(&search_key);
    if (found_tex)
    {
        slImageTexRef ref_out = found_tex;
        SDL_UnlockMutex(slTexListMutex);
        return ref_out;
    }

    slImageTexture* out = new slImageTexture(search_key);

    out->Reserve(); // Avoid GC while the texture is still loading.
    slImageAsyncQueue.Push(out);

    slImageTexRef ref_out = out;
    SDL_UnlockMutex(slTexListMutex);
	return ref_out;
}
static slTextTexRef slTextTexture::Render (char* str, SDL_Color color, slFont* font)
{
	if (!str) str = "";

    SDL_LockMutex(slTexListMutex);

    slTextInfo search_key(str,font,color,{0,0,0,0});
    slTextTexture* found_tex = slTextTexturesTree.Lookup(&search_key);
    if (found_tex)
    {
        slTextTexRef ref_out = found_tex;
        SDL_UnlockMutex(slTexListMutex);
        return ref_out;
    }

	slTextTexture* out = new slTextTexture(search_key);

    out->Reserve(); // Avoid GC while the texture is still loading.
    slTextAsyncQueue.Push(out);

    slTextTexRef ref_out = out;
    SDL_UnlockMutex(slTexListMutex);
	return ref_out;
}

static slMemoryTexRef slMemoryTexture::Create ()
{
    SDL_LockMutex(slTexListMutex);
    slMemoryTexRef ref_out = new slMemoryTexture;
    SDL_UnlockMutex(slTexListMutex);
    return ref_out;
}
void slMemoryTexture::Update (slInt2 wh, void* colorbuf)
{
    aspect = wh.w / (slScalar)wh.h;
    dims_valid = true;
    ready = false;

    // The RGBA component masks don't matter because the only purpose of the
    // surface is to just hold pixel data for the upload function. Still, might
    // as well set them up properly anyway, there is zero cost to doing so.
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    SDL_Surface* surf = SDL_CreateRGBSurface(0,wh.w,wh.h,32,0xFF<<24,0xFF<<16,0xFF<<8,0xFF);
#else
    SDL_Surface* surf = SDL_CreateRGBSurface(0,wh.w,wh.h,32,0xFF,0xFF<<8,0xFF<<16,0xFF<<24);
#endif
    memcpy(surf->pixels,colorbuf,wh.w * wh.h * 4);

    Reserve(); // Prevent GC during upload.
    slQueueTexUpload(this,surf);
}

void slTargetTexture::InternalCreate ()
{
    printf("Creating slTargetTexture: %ux%u\n",(unsigned)dims.w,(unsigned)dims.h);
    aspect = dims.w / (slScalar)dims.h;

    glGenFramebuffers(1,&framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER,framebuffer);
    glGenTextures(1,&tex);
    glBindTexture(GL_TEXTURE_2D,tex);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,dims.w,dims.h,0,GL_RGBA,GL_UNSIGNED_BYTE,NULL);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glGenRenderbuffers(1,&depth_renderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depth_renderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,dims.w,dims.h);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depth_renderbuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,tex,0);
    glDrawBuffers(sizeof(draw_buffers)/sizeof(*draw_buffers),draw_buffers);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        slFatal("couldn't set up framebuffer",868);
}
void slTargetTexture::InternalDestroy ()
{
    glDeleteFramebuffers(1,&framebuffer);
    glDeleteRenderbuffers(1,&depth_renderbuffer);
}
slTargetTexture::slTargetTexture (slInt2 wh)
{
    dims = wh;
    InternalCreate();
    dims_valid = true;
    ready = true;
}
slTargetTexture::~slTargetTexture ()
{
    InternalDestroy();
}
static slTargetTexRef slTargetTexture::Create (slInt2 wh)
{
    if (wh.w < 1) wh.w = 1;
    if (wh.h < 1) wh.h = 1;
    SDL_LockMutex(slTexListMutex);
    slTargetTexRef ref_out = new slTargetTexture(wh);
    SDL_UnlockMutex(slTexListMutex);
    return ref_out;
}
static slTargetTexRef slTargetTexture::Create ()
{
    return Create(slGetWindowResolution());
}
static slTargetTexRef slTargetTexture::Create (slVec2 wh)
{
    slInt2 area = slVec2(slGetWindowResolution()) * wh;
    return Create(area);
}
void slTargetTexture::Resize (slInt2 wh)
{
    // Canvas size: (wh.w, wh.h) pixels.
    if (wh.w < 1) wh.w = 1;
    if (wh.h < 1) wh.h = 1;
    if (dims != wh)
    {
        InternalDestroy();
        dims = wh;
        InternalCreate();
    }
}
void slTargetTexture::Resize ()
{
    // Canvas size: (screen.x, screen.y) pixels.
    Resize(slGetWindowResolution());
}
void slTargetTexture::Resize (slVec2 wh)
{
    // Canvas size: (screen.x * wh.w, screen.y * wh.h) pixels.
    slInt2 area = slVec2(slGetWindowResolution()) * wh;
    Resize(area);
}
void slTargetTexture::DrawTo ()
{
    glBindFramebuffer(GL_FRAMEBUFFER,framebuffer);
    glViewport(0,0,dims.w,dims.h);
}
void slDrawToScreen ()
{
    slInt2 area = slGetWindowResolution();
    glBindFramebuffer(GL_FRAMEBUFFER,0);
    glViewport(0,0,area.w,area.h);
}

void slReleaseTexture (slTexture* todel)
{
	if (todel->tex) glDeleteTextures(1,&todel->tex);
    delete todel;
}
void slDoomTexture (slTexture* tex)
{
    SDL_LockMutex(slTexListMutex);
    /*  Must double-check the atomic to ensure that, between checking first
        time and obtaining the mutex, the usage has not become nonzero.  */
    if (!SDL_AtomicGet(&tex->usages))
    {
        //printf("usage hit zero on %llX\n",tex);
        tex->Unregister();
        slBackgroundUploader.Push(slReleaseTexture,tex,&slTexturesQueuedForGC);
    }
    SDL_UnlockMutex(slTexListMutex);
}
void slTexture::Abandon ()
{
    if (SDL_AtomicDecRef(&usages))
        slDoomTexture(this);
}



namespace sl_internal
{

void slTexturesInit ()
{
	slCreateInvalidTexture();

// The auto shrinking could happen during threaded list adds/removes...
// which would be very dangerous.
//	slTextures.HookAutoShrink();
	//slGC_HookAction(slShrinkTexReplaceQueue);

	slMutex_libfreetype = SDL_CreateMutex();
	slMutex_IMG_Load = SDL_CreateMutex();
    slTexListMutex = SDL_CreateMutex();

    slTextAsyncQueue.Init();
    slImageAsyncQueue.Init();
};
void slTexturesQuit ()
{
    slTextAsyncQueue.Quit();
    slImageAsyncQueue.Quit();
    slWaitBackgroundUploads(&slTexturesInFinalization);

    // Can destroy existing textures in current thread, but use slDoomTexture
    // since doing anything differently is just a maintenance hazard.
	slTextures.UntilEmpty(slDoomTexture);
	// Wait until background destruction is finished.
    slNextFrameTasks.Flush();
    slWaitBackgroundUploads(&slTexturesQueuedForGC);

	SDL_DestroyMutex(slMutex_libfreetype);
	SDL_DestroyMutex(slMutex_IMG_Load);
    SDL_DestroyMutex(slTexListMutex);

	slFreeInvalidTexture();

	slFonts.Clear(slDeallocFont);
};

}



void slStepTexSwapChain (slTexSwapChain* chain)
{
	slTexReplacement* toplevel = chain->first;
	if (!toplevel) return;
	slTexReplacement* latest_valid = NULL;
	slTexReplacement* current_level = toplevel;
	while (current_level)
	{
		// Null pointers will not be queued.
		/*if (!current_level->replacement) latest_valid = current_level;
		else*/ if (current_level->replacement->ready) latest_valid = current_level;
		current_level = current_level->next;
	};
	//printf("Latest Valid: %llX\n",latest_valid);
	if (!latest_valid) return;
	slTexture* replaced = chain->texref;
	chain->texref = latest_valid->replacement;
	chain->first = latest_valid->next;
	if (replaced) replaced->Abandon();
	while (toplevel != latest_valid)
	{
		current_level = toplevel;
		toplevel = toplevel->next;
		current_level->replacement->Abandon();
		free(current_level);
	};
	free(latest_valid);
};
bool slTexInSwapChain (slTexSwapChain* chain, slTexture* tex)
{
	//return true;
	if (chain->texref == tex) return true;
	//printf("searching swap chain for texture\n");
	slTexReplacement* swap = chain->first;
	while (swap)
	{
		if (swap->replacement == tex) return true;
		swap = swap->next;
	};
	return false;
};
void slClearTexSwapChain (slTexSwapChain* chain)
{
	//printf("Clearing texture swap chain.\n");
	//return;
	slTexReplacement* swap = chain->first;
	while (swap)
	{
		slTexReplacement* next = swap->next;
		//slSingleTextureGC(swap->replacement,NULL);
		swap->replacement->Abandon();
		free(swap);
		swap = next;
		//printf("freed swap entry\n");
	};
	chain->first = NULL;
	//printf("cleared swap chain\n");
};
void slQuitTexSwapChain (slTexSwapChain* chain)
{
	slClearTexSwapChain(chain);
	if (chain->texref) chain->texref->Abandon();
};
void slQueueTexSwap (slTexSwapChain* chain, slTexRef to_swap_ref)
{
    slTexture* to_swap = to_swap_ref.raw_ref;

	if (to_swap == chain->texref)
	{
		//printf("Setting texture to itself, just clearing swap chain.\n");
		slClearTexSwapChain(chain); // Remove anything queued before this, which is anything queued at all.
		return;
	};
	if (to_swap)
	{
		if (!to_swap->ready)
		{
			if (chain->texref /*|| chain->first*/) goto NOT_INSTANT; // Usages counter will or won't be incremented there.
			// If there isn't a texref, we can set it instantly, even to something that's not ready.
			// If there isn't a texref, there cannot be a queue (since anything queued would have replaced the texref, first.
		};
		// Exists, and instant swap, so definitely need to increment the usages counter.
        to_swap->Reserve();
	};
	//printf("Setting texture instantly. It is %llX\n",to_swap);
	// Doesn't exist (or if it does, is ready) so we can bypass the queue entirely.


{
	slClearTexSwapChain(chain);
	slTexture* replaced = chain->texref;
	chain->texref = to_swap;
	if (replaced) replaced->Abandon();
	return;
}

	NOT_INSTANT:
	//printf("Not instant, searching the swap nodes.\n");
	slTexReplacement** swap_ptr = &chain->first;
	while (*swap_ptr)
	{
		if ((*swap_ptr)->replacement == to_swap) goto REMOVETRAIL;
		swap_ptr = &((*swap_ptr)->next);
	};
	//printf("Was not in the queue, appending to end.\n");
	/// It wasn't already in the queue. Increment usages.
{
    to_swap->Reserve();
	slTexReplacement* out = malloc(sizeof(slTexReplacement));
	out->replacement = to_swap;
	out->next = NULL;
	*swap_ptr = out;
	return;
}

	REMOVETRAIL:
	//printf("Was already in queue, removing nodes beyond.\n");
	/// It was already in the queue. Do not increment the usages.
	slTexReplacement* finalnode = *swap_ptr;
	swap_ptr = &finalnode->next;
	while (*swap_ptr)
	{
		slTexReplacement* pastfinalnode = *swap_ptr;
		swap_ptr = &((*swap_ptr)->next);
		pastfinalnode->replacement->Abandon();
		free(pastfinalnode);
	};
	finalnode->next = NULL;
};
slTexSwapChain slInitTexSwapChain ()
{
	return {NULL,NULL};
};
