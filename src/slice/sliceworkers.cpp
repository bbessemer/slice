#include <slice.h>

struct slSchedCall
{
	void (*func) (void*);
	void* arg;
};
struct slWorker
{
	SDL_Thread* thread;
	union
	{
		void** items; // used only in list mode
		void* array; // used only in array mode
		slBU start; // used only in simple & job mode
	};
	slBU itemcount;
	SDL_sem* sem;

	slSchedCall* sch_array;
	slBU sch_capacity;
	slBU sch_usage;
	//slGC_Action* shrink_action;
	slBU sch_max_usage;

	slBU cpu_id;

	slGC_Action shrink_action;
};
slBU slWorkerQueueAllocSize = 64;
void slSchedule (void (*func) (void* arg), void* arg, slWorker* worker)
{
	if (worker->sch_usage >= worker->sch_capacity)
		worker->sch_array = realloc(worker->sch_array,sizeof(slSchedCall) * (worker->sch_capacity += slWorkerQueueAllocSize));
	worker->sch_array[worker->sch_usage++] = (slSchedCall){func,arg};
};
#define slWorkerMode_List 0
#define slWorkerMode_Array 1
#define slWorkerMode_Job 2
#define slWorkerMode_Simple 3
struct
{
	void* userdata;
	union
	{
		void (*peritem) (void* item, void* userdata, slWorker* worker); // not used in job mode
		slBulkJob* job;
		void (*work_func) (slBU start, slBU end, void* userdata, slWorker* worker);
	};
	SDL_sem* done;
	SDL_atomic_t remaining;
	slBU item_size; // Size of items in array mode.
	Uint8 mode;
	bool die = false;
}
slWorkersGlobal;
slWorker* slWorkers;
slBU slWorkerCount;
void slFlushSchedules (slBU worker_count)
{
	for (slBU cur = 0; cur < worker_count; cur++)
	{
		slWorker* worker = slWorkers + cur;
		slSchedCall* calls = worker->sch_array;
		slBU count = worker->sch_usage;
		worker->sch_usage = 0;
		if (worker->sch_max_usage < count) worker->sch_max_usage = count;
		for (slBU i = 0; i < count; i++) calls[i].func(calls[i].arg);
	};
};
void slShrinkSchedule (slWorker* worker)
{
	/*if (worker->sch_array)
	{
		free(worker->sch_array);
		worker->sch_array = NULL;
		worker->sch_capacity = 0;
	};*/
	slBU remove = ((worker->sch_capacity - worker->sch_max_usage) / slWorkerQueueAllocSize) * slWorkerQueueAllocSize;
	if (remove) worker->sch_array = realloc(worker->sch_array,sizeof(slSchedCall) * (worker->sch_capacity -= remove));
	worker->sch_max_usage = 0;
};
slBU slCeil16 (slBU size)
{
	Uint8 overhang = size & 0xF;
	if (overhang) size += 0x10 - overhang;
	return size;
};
//slList slBulkJobs = slListInit("Slice Bulk Jobs",slNoIndex);
slBulkJob* slCreateBulkJob (slBulkJobDef def)
{
	def.thread_userdata_size = slCeil16(def.thread_userdata_size);
	slBU thread_data_count = slWorkerCount;
	if (!thread_data_count) thread_data_count = 1;
	slBulkJob* job = malloc(sizeof(slBulkJob) + def.thread_userdata_size * thread_data_count);
	//slListAdd(&slBulkJobs,job);
	job->def = def;
	if (def.init_thread_userdata)
	{
		void* udata = (void*)job + sizeof(slBulkJob);
		for (slBU cur = 0; cur < thread_data_count; cur++, udata += def.thread_userdata_size)
			def.init_thread_userdata(udata);
	};
	return job;
};
void slDestroyBulkJob (slBulkJob* job)
{
	if (job->def.quit_thread_userdata)
	{
		slBU thread_data_count = slWorkerCount;
		if (!thread_data_count) thread_data_count = 1;
		void* udata = (void*)job + sizeof(slBulkJob);
		for (slBU cur = 0; cur < thread_data_count; cur++, udata += job->def.thread_userdata_size)
			job->def.quit_thread_userdata(udata);
	};
	//slListRemove(&slBulkJobs,job);
	free(job);
};
int slWorkerProc (slWorker* self)
{
	srand(time(NULL) + self->cpu_id); // Thread local random seed.

	//slUseContext(); /// Make workers able to run OpenGL calls. <- doesnt actually work

	while (true)
	{
		SDL_SemWait(self->sem);
		asm volatile("": : :"memory");


		if (slWorkersGlobal.die) return 0;
		//Uint64 timenow = SDL_GetPerformanceCounter();
		if (slWorkersGlobal.mode == slWorkerMode_List)
			for (slBU cur = 0; cur < self->itemcount; cur++) slWorkersGlobal.peritem(*(self->items + cur),slWorkersGlobal.userdata,self);
		else if (slWorkersGlobal.mode == slWorkerMode_Simple)
			slWorkersGlobal.work_func(self->start,self->start + self->itemcount,slWorkersGlobal.userdata,self);
		else if (slWorkersGlobal.mode == slWorkerMode_Array)
			for (slBU cur = 0; cur < self->itemcount; cur++) slWorkersGlobal.peritem(self->array + cur * slWorkersGlobal.item_size,slWorkersGlobal.userdata,self);
		//Uint64 timeafter = SDL_GetPerformanceCounter();
		//printf("Worker Cycles: %llu\n",timeafter - timenow);
		else
		{
			void* thread_userdata = (void*)slWorkersGlobal.job + sizeof(slBulkJob) + slWorkersGlobal.job->def.thread_userdata_size * self->cpu_id;
			slWorkersGlobal.job->def.thread_work(self->start,self->itemcount,thread_userdata,slWorkersGlobal.userdata,self);
		};






		if (SDL_AtomicDecRef(&slWorkersGlobal.remaining)) SDL_SemPost(slWorkersGlobal.done);
	};
};
inline void slInitWorkerQueue (slWorker* worker)
{
	worker->sch_array = NULL;
	worker->sch_capacity = 0;
	worker->sch_usage = 0;
	worker->sch_max_usage = 0;
	worker->shrink_action.Hook(slShrinkSchedule,worker);
};
inline void slQuitWorkerQueue (slWorker* worker)
{
	if (worker->sch_array) free(worker->sch_array);
	//slGC_UnhookAction(worker->shrink_action);
};
SDL_mutex* slBulkWorkerMutex;



namespace sl_internal
{

void slWorkersInit ()
{
	slBulkWorkerMutex = SDL_CreateMutex();
	slWorkerCount = SDL_GetCPUCount();
	if (slWorkerCount == 1)
	{
		slWorkerCount = 0; // Single-Core CPU: Don't waste time with threading overhead.

		slWorkers = malloc(sizeof(slWorker)); // Make this anyway for queue properties.
		slInitWorkerQueue(slWorkers);
	}
	else
	{
		SDL_AtomicSet(&slWorkersGlobal.remaining,0); /// This is new. Is it needed?
		slWorkers = malloc(sizeof(slWorker) * slWorkerCount);
		slWorkersGlobal.done = SDL_CreateSemaphore(0);
		for (slBU cur = 0; cur < slWorkerCount; cur++)
		{
			slWorker* worker = slWorkers + cur;
			worker->sem = SDL_CreateSemaphore(0);
			worker->thread = SDL_CreateThread(slWorkerProc,"Bulk Handler",worker);
			slInitWorkerQueue(worker);
			worker->cpu_id = cur;
		};
	};
};
void slWorkersQuit ()
{
	//while (slBulkJobs.itemcount) slDestroyBulkJob(*slBulkJobs.items);
	if (slWorkerCount)
	{
		slWorkersGlobal.die = true;
		for (slBU cur = 0; cur < slWorkerCount; cur++)
		{
			slWorker* worker = slWorkers + cur;
			SDL_SemPost(worker->sem);
			SDL_WaitThread((slWorkers + cur)->thread,NULL);
			SDL_DestroySemaphore(worker->sem);
			slQuitWorkerQueue(worker);
		};
		SDL_DestroySemaphore(slWorkersGlobal.done);
	}
	else slQuitWorkerQueue(slWorkers); // One queue to clean up even in single thread mode.
	free(slWorkers); // Worker is created even for single core CPU (for queue).
	SDL_DestroyMutex(slBulkWorkerMutex);
};

}

__thread slWorker* slLocalSingleThreadWorker = NULL;
inline void slVerifyExistsLocalSingleThreadWorker ()
{
	if (!slLocalSingleThreadWorker)
	{
		slLocalSingleThreadWorker = malloc(sizeof(slWorker));
		slInitWorkerQueue(slLocalSingleThreadWorker);
		SDL_TLSSet(SDL_TLSCreate(),slLocalSingleThreadWorker,slQuitWorkerQueue);
	};
};
inline void slFlushLocalSingleThreadWorker ()
{
	for (slBU cur = 0; cur < slLocalSingleThreadWorker->sch_usage; cur++)
	{
		slSchedCall call = slLocalSingleThreadWorker->sch_array[cur];
		call.func(call.arg);
	};
	if (slLocalSingleThreadWorker->sch_usage > slLocalSingleThreadWorker->sch_max_usage)
		slLocalSingleThreadWorker->sch_max_usage = slLocalSingleThreadWorker->sch_usage;
	slLocalSingleThreadWorker->sch_usage = 0;
};
void slExecuteBulkJob (slBulkJob* job, slBU count, void* main_userdata, slBU threading_threshold)
{
	//if (!count) return;
	// Always do the start & finish functions.

	void* all_userdata = (void*)job + sizeof(slBulkJob);
	if (slWorkerCount && count >= threading_threshold)
	{
		if (job->def.main_prework) job->def.main_prework(all_userdata,job->def.thread_userdata_size,main_userdata,slWorkerCount);
		slBU perthread = (count / slWorkerCount) + 1;

		SDL_LockMutex(slBulkWorkerMutex);

		SDL_AtomicSet(&slWorkersGlobal.remaining,slWorkerCount);
		slWorkersGlobal.job = job;
		slWorkersGlobal.userdata = main_userdata;
		slWorkersGlobal.mode = slWorkerMode_Job;
		for (slBU cur = 0; cur < slWorkerCount; cur++)
		{
			slWorker* worker = slWorkers + cur;
			slBU offset = perthread * cur;
			worker->start = offset;
			if (offset >= count) worker->itemcount = 0;
			else if (offset + perthread >= count) worker->itemcount = count - offset;
			else worker->itemcount = perthread;
			SDL_SemPost(worker->sem);
		};
		SDL_SemWait(slWorkersGlobal.done);
		slFlushSchedules(slWorkerCount); // Do stuff the workers have scheduled to happen upon completion.

		SDL_UnlockMutex(slBulkWorkerMutex);

		if (job->def.main_postwork) job->def.main_postwork(all_userdata,job->def.thread_userdata_size,main_userdata,slWorkerCount);
	}
	else
	{
		// Single-Core CPU: Don't waste time with threading overhead.
		if (job->def.main_prework)
			job->def.main_prework(all_userdata,job->def.thread_userdata_size,main_userdata,1);
		slVerifyExistsLocalSingleThreadWorker();
		job->def.thread_work(0,count,all_userdata,main_userdata,/*slWorkers*//*&temp_worker*/slLocalSingleThreadWorker);
		slFlushLocalSingleThreadWorker();
		if (job->def.main_postwork)
			job->def.main_postwork(all_userdata,job->def.thread_userdata_size,main_userdata,1);
	};
};
void slDoArrayWork (void* array, slBU count, slBU size, void (*peritem) (void* item, void* userdata, slWorker* worker), void* userdata, slBU threading_threshold)
{
	if (!count) return;
	if (slWorkerCount && count >= threading_threshold)
	{
		slBU perthread = (count / slWorkerCount) + 1;

		SDL_LockMutex(slBulkWorkerMutex);

		SDL_AtomicSet(&slWorkersGlobal.remaining,slWorkerCount);
		slWorkersGlobal.peritem = peritem;
		slWorkersGlobal.userdata = userdata;
		slWorkersGlobal.mode = slWorkerMode_Array;
		slWorkersGlobal.item_size = size;
		for (slBU cur = 0; cur < slWorkerCount; cur++)
		{
			slWorker* worker = slWorkers + cur;
			slBU offset = perthread * cur;
			worker->items = array + offset * size;
			if (offset >= count) worker->itemcount = 0;
			else if (offset + perthread >= count) worker->itemcount = count - offset;
			else worker->itemcount = perthread;
			SDL_SemPost(worker->sem);
		};
		SDL_SemWait(slWorkersGlobal.done);
		slFlushSchedules(slWorkerCount); // Do stuff the workers have scheduled to happen upon completion.

		SDL_UnlockMutex(slBulkWorkerMutex);
	}
	else
	{
		slVerifyExistsLocalSingleThreadWorker();
		for (slBU cur = 0; cur < count; cur++) peritem(array + cur * size,userdata,slLocalSingleThreadWorker); // Single-Core CPU: Don't waste time with threading overhead.
		slFlushLocalSingleThreadWorker();
	};
};
void slDoSimpleWork (slBU count, void (*work_func) (slBU start, slBU end, void* userdata, slWorker* worker), void* userdata, slBU threading_threshold = 16)
{
	if (!count) return;
	if (slWorkerCount && count >= threading_threshold)
	{
		slBU perthread = (count / slWorkerCount) + 1;

		SDL_LockMutex(slBulkWorkerMutex);

		SDL_AtomicSet(&slWorkersGlobal.remaining,slWorkerCount);
		slWorkersGlobal.work_func = work_func;
		slWorkersGlobal.userdata = userdata;
		slWorkersGlobal.mode = slWorkerMode_Simple;
		for (slBU cur = 0; cur < slWorkerCount; cur++)
		{
			slWorker* worker = slWorkers + cur;
			slBU offset = perthread * cur;
			worker->start = offset;
			if (offset >= count) worker->itemcount = 0;
			else if (offset + perthread >= count) worker->itemcount = count - offset;
			else worker->itemcount = perthread;
			SDL_SemPost(worker->sem);
		};
		SDL_SemWait(slWorkersGlobal.done);
		slFlushSchedules(slWorkerCount); // Do stuff the workers have scheduled to happen upon completion.

		SDL_UnlockMutex(slBulkWorkerMutex);
	}
	else
	{
		slVerifyExistsLocalSingleThreadWorker();
		work_func(0,count,userdata,slLocalSingleThreadWorker); // Single-Core CPU: Don't waste time with threading overhead.
		slFlushLocalSingleThreadWorker();
	};
};
void slDoWork (slList* list, void (*peritem) (void* item, void* userdata, slWorker* worker), void* userdata, slBU threading_threshold)
{
	if (!list->itemcount) return;
	if (slWorkerCount && list->itemcount >= threading_threshold)
	{
		slBU perthread = (list->itemcount / slWorkerCount) + 1;

		SDL_LockMutex(slBulkWorkerMutex);

		SDL_AtomicSet(&slWorkersGlobal.remaining,slWorkerCount);
		slWorkersGlobal.peritem = peritem;
		slWorkersGlobal.userdata = userdata;
		slWorkersGlobal.mode = slWorkerMode_List;
		slBU cur;
		for (cur = 0; cur < slWorkerCount; cur++)
		{
			slWorker* worker = slWorkers + cur;
			slBU offset = perthread * cur;
			worker->items = list->items + offset;
			if (offset >= list->itemcount) worker->itemcount = 0;
			else if (offset + perthread >= list->itemcount) worker->itemcount = list->itemcount - offset;
			else worker->itemcount = perthread;
			SDL_SemPost(worker->sem);
		};
		SDL_SemWait(slWorkersGlobal.done);
		slFlushSchedules(slWorkerCount); // Do stuff the workers have scheduled to happen upon completion.

		SDL_UnlockMutex(slBulkWorkerMutex);
	}
	else
	{
		slVerifyExistsLocalSingleThreadWorker();
		for (slBU cur = 0; cur < list->itemcount; cur++) peritem(*(list->items + cur),userdata,slLocalSingleThreadWorker); // Single-Core CPU: Don't waste time with threading overhead.
		slFlushLocalSingleThreadWorker();
	};
};
