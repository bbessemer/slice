#include <slice3d.h>
#include <slice3d/load-obj.h>
using namespace s3d_internal;


#ifdef __APPLE__
void sincosf(float x, float *s, float *c)
{
	*s = sinf(x);
	*c = cosf(x);
}
#endif

void GLmat4::print ()
{
	printf(
		"\n"
		"     |\t%.3f\t%.3f\t%.3f\t%.3f\t|\n"
		"     |\t%.3f\t%.3f\t%.3f\t%.3f\t|\n"
		"     |\t%.3f\t%.3f\t%.3f\t%.3f\t|\n"
		"     |\t%.3f\t%.3f\t%.3f\t%.3f\t|\n"
		"\n",
		data[0],data[1],data[2],data[3],
		data[4],data[5],data[6],data[7],
		data[8],data[9],data[10],data[11],
		data[12],data[13],data[14],data[15]
	);
};
void GLvec4::print ()
{
	printf("     (\t%.3f,\t%.3f,\t%.3f,\t%.3f\t)\n",x,y,z,w);
};
GLmat4 GLmat4_mul (GLmat4& a, GLmat4& b)
{
	GLvec4_raw col1 = { b.data[0], b.data[4], b.data[8],  b.data[12] };
	GLvec4_raw col2 = { b.data[1], b.data[5], b.data[9],  b.data[13] };
	GLvec4_raw col3 = { b.data[2], b.data[6], b.data[10], b.data[14] };
	GLvec4_raw col4 = { b.data[3], b.data[7], b.data[11], b.data[15] };
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		GLvec4(a.row1 * col1).sum4(),	GLvec4(a.row1 * col2).sum4(),	GLvec4(a.row1 * col3).sum4(),	GLvec4(a.row1 * col4).sum4(),
		GLvec4(a.row2 * col1).sum4(),	GLvec4(a.row2 * col2).sum4(),	GLvec4(a.row2 * col3).sum4(),	GLvec4(a.row2 * col4).sum4(),
		GLvec4(a.row3 * col1).sum4(),	GLvec4(a.row3 * col2).sum4(), 	GLvec4(a.row3 * col3).sum4(), 	GLvec4(a.row3 * col4).sum4(),
		GLvec4(a.row4 * col1).sum4(),	GLvec4(a.row4 * col2).sum4(), 	GLvec4(a.row4 * col3).sum4(),	GLvec4(a.row4 * col4).sum4()
	};
	return out;
};
#define GLmat4_swap(a,b) temp = data[a]; data[a] = data[b]; data[b] = temp;
GLmat4 GLmat4::transpose ()
{
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		data[0],	data[4],	data[8],	data[12],
		data[1],	data[5],	data[9],	data[13],
		data[2],	data[6],	data[10],	data[14],
		data[3],	data[7],	data[11],	data[15]
	};
	return out;
};
void GLmat4::transpose_in_place ()
{
	GLfloat temp;
	GLmat4_swap(1,4)
	GLmat4_swap(2,8)
	GLmat4_swap(3,12)
	GLmat4_swap(6,9)
	GLmat4_swap(7,13)
	GLmat4_swap(11,14)
};
GLmat4 GLmat4_scaling (GLvec4 scalevec)
{
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		scalevec.x,		0,				0,				0,
		0,				scalevec.y,		0,				0,
		0,				0,				scalevec.z,		0,
		0,				0,				0,				scalevec.w
	};
	return out;
};
GLmat4 GLmat4_xrotate (GLfloat rads)
{
	GLmat4 out;
	/**
		Y component is not affected by X, since we're rotating around it
		Y component begins to include Z, so use cosine
		Y component stops including itself, so use sine

		Actually cosine starts at one and sine starts are zero, so invert all these
	**/
	GLfloat sinf_rads = sinf(rads);
	GLfloat cosf_rads = cosf(rads);
	out.entire_vec = (GLvec16_raw){
		1,				0,				0,				0,
		0,				cosf_rads,		-sinf_rads,		0,
		0,				sinf_rads,		cosf_rads,		0,
		0,				0,				0,				1
	};
	return out;
};
GLmat4 GLmat4_yrotate (GLfloat rads)
{
	GLmat4 out;
	/**
		X component not affected by Y since we're rotating around it.
		X component begins to include Z so use cosine
	**/
	GLfloat sinf_rads = sinf(rads);
	GLfloat cosf_rads = cosf(rads);
	out.entire_vec = (GLvec16_raw)
	{
		cosf_rads,		0,				-sinf_rads,		0,
		0,				1,				0,				0,
		sinf_rads,		0,				cosf_rads,		0,
		0,				0,				0,				1
	};
	return out;
};
GLmat4 GLmat4_zrotate (GLfloat rads)
{
	GLmat4 out;
	/**
		X not affected by Z, rotating around
		X begins to have Y so use cosine
	**/
	GLfloat sinf_rads = sinf(rads);
	GLfloat cosf_rads = cosf(rads);
	out.entire_vec = (GLvec16_raw)
	{
		cosf_rads,		-sinf_rads,		0,				0,
		sinf_rads,		cosf_rads,		0,				0,
		0,				0,				1,				0,
		0,				0,				0,				1
	};
	return out;
};
GLmat4 GLmat4_offset (GLfloat x, GLfloat y, GLfloat z)
{
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		1,				0,				0,				x,
		0,				1,				0,				y,
		0,				0,				1,				z,
		0,				0,				0,				1
	};
	return out;
};
GLmat4 GLmat4_ortho (GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat near_clip, GLfloat far_clip)
{
	GLfloat dx = left - right;
	GLfloat dy = bottom - top;
	GLfloat dz = near_clip - far_clip;
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		-2 / dx,	0,			0,			(right + left) / dx,
		0,			-2 / dy,	0,			(top + bottom) / dy,
		0,			0,			2 / dz,		(far_clip + near_clip) / dz,
		0,			0,			0,			1
	};
	return out;
};
/*GLmat4 GLmat4_ortho_depth (GLfloat near_z, GLfloat far_z)
{
	GLmat4 out;
	//GLfloat zm = 2 / (near_z - far_z);
	//GLfloat za = (far_z + near_z) / (near_z - far_z);
	out.entire_vec = (GLvec16_raw)
	{
		1,	0,	0,	0,
		0,	1,	0,	0,
		0,	0,	1,	0,//za,
		0,	0,	0,	1
	};
	return out;
};*/
slForceInline inline GLfloat cotangent (GLfloat x)
{
	GLfloat sinval,cosval;
	sincosf(x,&sinval,&cosval);
	return cosval / sinval;
};
GLmat4 GLmat4_perspective (GLfloat fov, GLfloat znear, GLfloat zfar)
{
	GLfloat f = cotangent(slDegToRad_F(fov) / 2);
	GLfloat zmul = (zfar + znear) / (znear - zfar);
	GLfloat zadd = (2 * zfar * znear) / (znear - zfar);
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		f,			0,			0,			0,
		0,			f,			0,			0,
		0,			0,			zmul,		zadd,
		0,			0,			-1,			0
	};
	return out;
};

slList s3dModels("Slice3D - Models",offsetof(s3dModel,_index_),false);
SDL_mutex* s3dModelListMutex;
SDL_atomic_t s3dModelsQueuedForGC = {0};

void s3dInitMeshBuffer (s3dMesh* mesh)
{
	glGenBuffers(1,&mesh->vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER,mesh->vertex_buffer);
}
void s3dInitModelTransforms_GL (s3dModel* model)
{
    // Create buffer.
    model->matrix_buffer_capacity = 1;
    glGenBuffers(1,&model->matrix_buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER,model->matrix_buffer);
    glBufferData(
        GL_COPY_WRITE_BUFFER,
        sizeof(GLmat4) * model->matrix_buffer_capacity,
        NULL, // No initial copy from memory.
        GL_DYNAMIC_DRAW
    );

    // Create buffer texture.
    glGenTextures(1,&model->matrix_texture);
    glBindTexture(GL_TEXTURE_BUFFER,model->matrix_texture);
    glTexBuffer(GL_TEXTURE_BUFFER,GL_RGBA32F,model->matrix_buffer);

}
void s3dInitModelTransforms (s3dModel* model)
{
    model->matrix_mutex = SDL_CreateMutex();
    model->matrix_count = 0;
    model->matrix_residency_capacity = 1;
    model->matrix_residency = malloc(sizeof(s3dObject*) * model->matrix_residency_capacity);
    slNextFrameTasks.Push(s3dInitModelTransforms_GL,model,NULL);
}
void s3dQuitModelTransforms (s3dModel* model)
{
    glDeleteTextures(1,&model->matrix_texture);
    glDeleteBuffers(1,&model->matrix_buffer);
}
s3dVec3 cross (s3dVec3 a, s3dVec3 b)
{
    /* todo: make this part of s3dVec3 */
    /* todo: vector math for performance */
    return s3dVec3(
        a.y * b.z - a.z * b.y,
        -(a.x * b.z - a.z * b.x),
        a.x * b.y - a.y * b.x
    );
}
void s3dMeshMaterialInfo (s3dMesh* outmesh, objMaterial* mat)
{
    outmesh->ambient_color      = mat->ambient_color;
    outmesh->diffuse_color      = mat->diffuse_color;
    outmesh->specular_color     = mat->specular_color;
    outmesh->specular_exponent  = mat->specular_exponent;
    outmesh->mag_filter_nearest = mat->mag_filter_nearest;
    outmesh->ignore_environment_ambient = mat->ignore_environment_ambient;

    outmesh->ambient_texref  = mat->ambient_texpath  ?  slLoadTexture(mat->ambient_texpath ).ReserveGetRaw() : NULL;
    outmesh->diffuse_texref  = mat->diffuse_texpath  ?  slLoadTexture(mat->diffuse_texpath ).ReserveGetRaw() : NULL;
    outmesh->specular_texref = mat->specular_texpath ?  slLoadTexture(mat->specular_texpath).ReserveGetRaw() : NULL;
    outmesh->opacity_texref  = mat->opacity_texpath  ?  slLoadTexture(mat->opacity_texpath ).ReserveGetRaw() : NULL;
    outmesh->normal_texref   = mat->bump_texpath     ? s3dLoadBumpMap(mat->bump_texpath    ).ReserveGetRaw() : NULL;
}
struct s3dObjLoadingData
{
    char* basepath_cloned;
    char* filename_cloned;

    s3dModel* output;
    objModel* input;
    float** each_mesh_vertices_buf;

    void* next;
};
void s3dFinalizeObj (s3dObjLoadingData* data)
{
    objModel* input = data->input;
    s3dModel* output = data->output;
    if (input)
    {
        for (int i = 0; i < input->group_count; i++)
        {
            s3dMesh* outmesh = output->meshes + i;
            float* vertices_buf = data->each_mesh_vertices_buf[i];
            objGroup* group = input->group_buf + i;

            s3dMeshMaterialInfo(outmesh,group->material);
    		s3dInitMeshBuffer(outmesh);
    		size_t vertices_size = sizeof(float) * 24 * outmesh->triangle_count;
            glBufferData(GL_ARRAY_BUFFER,vertices_size,vertices_buf,GL_STATIC_DRAW);
            free(vertices_buf);
            glFinish();
            output->meshcount++;
        }
        free(data->each_mesh_vertices_buf);
        objFree(input);
    }
    output->Abandon(); // Called ->Reserve() when loading began.
    free(data);
}
SDL_atomic_t s3dModelsInFinalization = {0};
class s3dObjLoadersProto : public slConsumerQueue
{
public:
    s3dObjLoadersProto () : slConsumerQueue(offsetof(s3dObjLoadingData,next)) {};
private:
    void ItemFunc (void* data_ptr) override
    {
        s3dObjLoadingData* data = data_ptr;

        objModel* input = objLoad(data->basepath_cloned,data->filename_cloned);
        free(data->basepath_cloned);
        free(data->filename_cloned);

        data->input = input;
        s3dModel* output = data->output;
        if (input)
        {
            output->meshes = malloc(sizeof(s3dMesh) * input->group_count);
            int* tricounts = malloc(sizeof(int) * input->group_count);
            for (int i = 0; i < input->group_count; i++)
            {
                objGroup* object = input->group_buf + i;
                int tricount = 0;
                for (int j = 0; j < object->face_count; j++)
                {
                    objFace* face = object->face_buf + j;
                    tricount += face->vertex_ref_count - 2;
                }
                tricounts[i] = tricount;
            }

            data->each_mesh_vertices_buf = malloc(sizeof(float*) * input->group_count);

            const int tri_floats = 8 * 3; // 3 vertices * {pos_xyz, tex_uv, norm_xyz}

            for (int obj_id = 0; obj_id < input->group_count; obj_id++)
            {
                objGroup* group = input->group_buf + obj_id;
                int tricount = tricounts[obj_id];
                float* vertices_buf = malloc(sizeof(float) * tri_floats * tricount);
                data->each_mesh_vertices_buf[obj_id] = vertices_buf;
                float* vertexpointer = vertices_buf;
                bool calc_normal = false;
                for (int fid = 0; fid < group->face_count; fid++)
                {
                    objFace* face = group->face_buf + fid;
                    for (int vertexid = 2; vertexid < face->vertex_ref_count; vertexid++)
                    {
                        bool should_calc_normal = false;
                        for (int id_offset = -2; id_offset <= 0; id_offset++)
                        {
                            // first vertex of each triangle should be the first vertex of
                            // the face, since we can assemble a 'triangle fan' in this way
                            objVertexRef* ref;
                            if (id_offset == -2) ref = face->vertex_ref_buf;
                            else ref = face->vertex_ref_buf + vertexid + id_offset;

                            should_calc_normal = should_calc_normal || !ref->normal_id;
                        }
                        s3dVec3 calculated_normal;
                        if (should_calc_normal)
                        {
                            s3dVec3 positions [3];
                            for (int id_offset = -2; id_offset <= 0; id_offset++)
                            {
                                // first vertex of each triangle should be the first vertex of
                                // the face, since we can assemble a 'triangle fan' in this way
                                objVertexRef* ref;
                                if (id_offset == -2) ref = face->vertex_ref_buf;
                                else ref = face->vertex_ref_buf + vertexid + id_offset;

                                // pos_id guaranteed to be >= 1
                                objVertexPos* pos = input->vertex_pos_buf + ref->pos_id - 1;
                                positions[id_offset + 2] = s3dVec3(pos->x,pos->y,pos->z);
                            }
                            s3dVec3 dir_1 = positions[1] - positions[0];
                            s3dVec3 dir_2 = positions[2] - positions[1];
                            calculated_normal = cross(dir_1,dir_2);
                        }

                        for (int id_offset = -2; id_offset <= 0; id_offset++)
                        {
                            // first vertex of each triangle should be the first vertex of
                            // the face, since we can assemble a 'triangle fan' in this way
                            objVertexRef* ref;
                            if (id_offset == -2) ref = face->vertex_ref_buf;
                            else ref = face->vertex_ref_buf + vertexid + id_offset;

                            // pos_id guaranteed to be >= 1
                            objVertexPos* pos = input->vertex_pos_buf + ref->pos_id - 1;
                            *vertexpointer++ = pos->x;
                            *vertexpointer++ = pos->y;
                            *vertexpointer++ = pos->z;
                            // we don't support position w component

                            // tex_id may be 0 if it was unspecified
                            if (ref->tex_id)
                            {
                                objVertexTex* tex = input->vertex_tex_buf + ref->tex_id - 1;
                                *vertexpointer++ = tex->u;
                                *vertexpointer++ = tex->v;
                                // we don't support texture w component
                            }
                            else
                            {
                                // Texture probably isn't used or is a solid color.
                                // Could leave this as uninitialized junk, or zeroes,
                                // but for bump mapping math to work, these actually
                                // need to be distinct for each vertex, even if they're
                                // just distinct junk values.
                                float junk = vertexpointer - vertices_buf;
                                *vertexpointer++ = junk;
                                *vertexpointer++ = junk;
                            }

                            // normal_id may be 0 if it was unspecified
                            if (ref->normal_id)
                            {
                                objVertexNormal* normal = input->vertex_normal_buf + ref->normal_id - 1;
                                *vertexpointer++ = normal->x;
                                *vertexpointer++ = normal->y;
                                *vertexpointer++ = normal->z;
                            }
                            else
                            {
                                *vertexpointer++ = calculated_normal.x;
                                *vertexpointer++ = calculated_normal.y;
                                *vertexpointer++ = calculated_normal.z;
                            }
                        }
                    }
                }

                s3dMesh* outmesh = output->meshes + obj_id;
                outmesh->triangle_count = tricount;
            }

            free(tricounts);
        }
        else
        {
            output->meshes = malloc(0);
        }

        slBackgroundUploader.Push(s3dFinalizeObj,data,&s3dModelsInFinalization);
    }
}
s3dObjLoaders;

struct s3dLoadedModelData
{
    s3dModel* model;
    char* filepath;
    slTreeNodeData tree_data;
};
struct s3dLoadedModelsTreeProto : slTreeSet
{
    int Compare (void* item_a, void* item_b) override
    {
        /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */
        s3dLoadedModelData* data_a = item_a;
        s3dLoadedModelData* data_b = item_b;
        return strcmp(data_a->filepath,data_b->filepath);
    }
    slTreeNodeData* GetTreeData (void* item) override
    {
        s3dLoadedModelData* data = item;
        return &data->tree_data;
    }
    void PrintItem (void* item) override
    {
        s3dLoadedModelData* data = item;
        printf("obj@'%s'\n",data->filepath);
    }
}
s3dLoadedModelsTree;
void s3dOnLoadedModelFree (s3dModel* model)
{
    s3dLoadedModelData* data = model->factory_data;

    s3dLoadedModelsTree.Remove(data);
    //printf("count:   %d\n",s3dLoadedModelsTree.total_items);
    //printf("recount: %d\n",s3dLoadedModelsTree.Recount());
    //s3dLoadedModelsTree.PrintInOrder();

    free(data->filepath);
    free(data);
}

s3dModelRef s3dLoadObj (char* basepath, char* filename)
{
    /* basepath can be NULL, filename expected to always exist */
    if (!basepath) basepath = "";
    char* fullpath = slStrAlwaysConcat(basepath,filename);

    SDL_LockMutex(s3dModelListMutex);

    s3dLoadedModelData search_data;
    search_data.filepath = fullpath;
    s3dLoadedModelData* found_data = s3dLoadedModelsTree.Lookup(&search_data);
    if (found_data)
    {
        s3dModelRef out_ref = found_data->model;
        SDL_UnlockMutex(s3dModelListMutex);
        free(fullpath);
        return out_ref;
    }

    s3dModel* output = malloc(sizeof(s3dModel));
    SDL_AtomicSet(&output->usages,0);

    s3dLoadedModelData* factory_data = malloc(sizeof(s3dLoadedModelData));
    factory_data->filepath = fullpath;

    factory_data->model = output;
    output->factory_data = factory_data;
    output->factory_free = s3dOnLoadedModelFree;

    s3dLoadedModelsTree.Insert(factory_data);
    //printf("count:   %d\n",s3dLoadedModelsTree.total_items);
    //printf("recount: %d\n",s3dLoadedModelsTree.Recount());
    //s3dLoadedModelsTree.PrintInOrder();

    s3dInitModelTransforms(output);

    output->Reserve(); // Implicitly prevent garbage collection until loaded.
    output->meshcount = 0;
    s3dObjLoadingData* loading_data = malloc(sizeof(s3dObjLoadingData));
    loading_data->basepath_cloned = slStrAlwaysClone(basepath);
    loading_data->filename_cloned = slStrAlwaysClone(filename);
    loading_data->output = output;
    s3dObjLoaders.Push(loading_data);

	s3dModels.Add(output);
    s3dModelRef ref_out = output;
    SDL_UnlockMutex(s3dModelListMutex);
	return ref_out;
}

s3dModelRef s3dSpriteModel (objMaterial material)
{
	s3dModel* model = malloc(sizeof(s3dModel));
    /*
        TO DO: Search existing sprite models to find one that had an identical
        material, and return it instead. (The factory_free won't need to free
        anything, but will need to remove from the sprite models collection).
    */
    model->factory_free = NULL;
	SDL_AtomicSet(&model->usages,0);

	model->meshes = malloc(sizeof(s3dMesh));
	s3dMesh* mesh = model->meshes;
	float vertices [48] =
	{
		-.5,-.5,0,	0,0,   0,0,1,
		 .5,-.5,0,	1,0,   0,0,1,
		 .5, .5,0,	1,1,   0,0,1,

		 .5, .5,0,	1,1,   0,0,1,
		-.5, .5,0,	0,1,   0,0,1,
		-.5,-.5,0,	0,0,   0,0,1,
	};
	mesh->triangle_count = 2;
	s3dInitMeshBuffer(mesh);
	glBufferData(GL_ARRAY_BUFFER,sizeof(vertices),vertices,GL_STATIC_DRAW);
    s3dMeshMaterialInfo(mesh,&material);
    model->meshcount = 1;
    /*
        To do: push the above loading code to slNextFrameTasks instead of doing
        it right there, so that this function can be called from any thread.
    */

    s3dInitModelTransforms(model);

    SDL_LockMutex(s3dModelListMutex);

	s3dModels.Add(model);
    s3dModelRef ref_out = model;

    SDL_UnlockMutex(s3dModelListMutex);

	return ref_out;
}
s3dModelRef s3dSpriteModel (char* sprite_texpath, bool mag_filter_nearest, float ambient, float diffuse, float specular, float spec_exp, bool ignore_environment_ambient)
{
    objMaterial mat = objFallbackMaterial;
    mat.ambient_texpath = sprite_texpath;
    mat.diffuse_texpath = sprite_texpath;
    mat.mag_filter_nearest = mag_filter_nearest;
    mat.ambient_color = {ambient,ambient,ambient};
    mat.diffuse_color = {diffuse,diffuse,diffuse};
    mat.specular_color = {specular,specular,specular};
    mat.specular_exponent = spec_exp;
    mat.ignore_environment_ambient = ignore_environment_ambient;
    return s3dSpriteModel(mat);
}

void s3dReleaseModel (s3dModel* model)
{
    //printf("releasing model %llX\n",model);

	s3dMesh* mesh = model->meshes;
	for (slBU cur = 0; cur < model->meshcount; cur++,mesh++)
	{
        if (mesh->ambient_texref) mesh->ambient_texref->Abandon();
		if (mesh->diffuse_texref) mesh->diffuse_texref->Abandon();
		if (mesh->specular_texref) mesh->specular_texref->Abandon();
        if (mesh->opacity_texref) mesh->opacity_texref->Abandon();
        if (mesh->normal_texref) mesh->normal_texref->Abandon();
		//free(mesh->vertices);
		glDeleteBuffers(1,&mesh->vertex_buffer);
	};
	free(model->meshes);

    SDL_DestroyMutex(model->matrix_mutex);
    free(model->matrix_residency);
    s3dQuitModelTransforms(model);

    free(model);
}
void s3dDoomModel (s3dModel* model)
{
    SDL_LockMutex(s3dModelListMutex);
    /*  Must double-check the atomic to ensure that, between checking first
        time and obtaining the mutex, the usage has not become nonzero.  */
    if (!SDL_AtomicGet(&model->usages))
    {
        //printf("usage hit zero on %llX\n",model);

	    s3dModels.Remove(model);
    	if (model->factory_free) model->factory_free(model);
        slBackgroundUploader.Push(s3dReleaseModel,model,&s3dModelsQueuedForGC);
    }
    SDL_UnlockMutex(s3dModelListMutex);
}
void s3dModel::Abandon ()
{
    if (SDL_AtomicDecRef(&usages))
        s3dDoomModel(this);
}
struct s3dModelMatrixUpdate
{
    s3dModel* model;
    int index;
    GLmat4 matrix;
    s3dModelMatrixUpdate (s3dModel* model, int index, GLmat4 matrix)
    {
        this->model = model;
        this->index = index;
        this->matrix = matrix;
        model->Reserve();
    }
    ~s3dModelMatrixUpdate ()
    {
        model->Abandon();
    }
};
void s3dModelMatricesEnforceBufferSize (s3dModel* model)
{
    if (model->matrix_count > model->matrix_buffer_capacity)
    {
        printf("expanding\n");

        // We will need the old buffer object and size to copy (and delete),
        slBU old_size = model->matrix_buffer_capacity;
        GLuint old_buffer = model->matrix_buffer;

        // Choose a power-of-2 size that is large enough.
        while (model->matrix_count > model->matrix_buffer_capacity)
            model->matrix_buffer_capacity <<= 1;

        // Copy data from the old buffer to a new one.
        glGenBuffers(1,&model->matrix_buffer);
        glBindBuffer(GL_COPY_READ_BUFFER,old_buffer);
        glBindBuffer(GL_COPY_WRITE_BUFFER,model->matrix_buffer);
        glBufferData(
            GL_COPY_WRITE_BUFFER,
            sizeof(GLmat4) * model->matrix_buffer_capacity,
            NULL, // No initial copy from memory.
            GL_DYNAMIC_DRAW
        );
        glCopyBufferSubData(
            GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER,
            0, 0, // No offset into either the read or write buffers.
            sizeof(GLmat4) * old_size
        );

        // Bind texture to new buffer.
        glBindTexture(GL_TEXTURE_BUFFER,model->matrix_texture);
        glTexBuffer(GL_TEXTURE_BUFFER,GL_RGBA32F,model->matrix_buffer);

        // Delete old buffer.
        glDeleteBuffers(1,&old_buffer);
    }
}
void s3dModelMatrixUpdateApply (s3dModelMatrixUpdate* update)
{
    /* Optimize for obviously-redundant updates. */
    if (update->index < update->model->matrix_count)
    {
        /* Make sure buffer is large enough. */
        s3dModelMatricesEnforceBufferSize(update->model);

        /* Apply update to the buffer data. */
        glBindBuffer(GL_COPY_WRITE_BUFFER,update->model->matrix_buffer);
        glBufferSubData(
            GL_COPY_WRITE_BUFFER,
            sizeof(GLmat4) * update->index,
            sizeof(GLmat4),
            &update->matrix
        );
    }

    delete update;
}
void s3dModel::UpdateMatrix (s3dObject* update_obj)
{
    SDL_LockMutex(matrix_mutex);

    slNextFrameTasks.Push(
        s3dModelMatrixUpdateApply,
        new s3dModelMatrixUpdate(
            this,
            update_obj->matrix_residence,
            update_obj->world_transform
        ),
        NULL
    );

    SDL_UnlockMutex(matrix_mutex);
}
void s3dModel::AddMatrix (s3dObject* add_obj)
{
    SDL_LockMutex(matrix_mutex);

    add_obj->matrix_residence = matrix_count++;
    if (matrix_count >= matrix_residency_capacity)
        matrix_residency = realloc(
            matrix_residency,
            sizeof(s3dObject**) * (matrix_residency_capacity <<= 1)
        );
    matrix_residency[add_obj->matrix_residence] = add_obj;

    slNextFrameTasks.Push(
        s3dModelMatrixUpdateApply,
        new s3dModelMatrixUpdate(
            this,
            add_obj->matrix_residence,
            add_obj->world_transform
        ),
        NULL
    );

    SDL_UnlockMutex(matrix_mutex);
}
void s3dModel::RemoveMatrix (s3dObject* remove_obj)
{
    SDL_LockMutex(matrix_mutex);

    s3dObject* last_obj = matrix_residency[--matrix_count];
    last_obj->matrix_residence = remove_obj->matrix_residence;
    matrix_residency[last_obj->matrix_residence] = last_obj;

    slNextFrameTasks.Push(
        s3dModelMatrixUpdateApply,
        new s3dModelMatrixUpdate(
            this,
            last_obj->matrix_residence,
            last_obj->world_transform
        ),
        NULL
    );

    SDL_UnlockMutex(matrix_mutex);
}

slList s3dObjects("Slice3D - Objects",offsetof(s3dObject,_index_),false);
slBlockAllocator s3dObjectAllocator ("Slice3D - Object Allocator",sizeof(s3dObject),256);
s3dObject* s3dCreateObject (s3dModelRef model_ref)
{
	s3dObject* out = s3dObjectAllocator.Allocate();
	s3dObjects.Add(out);
	out->model = model_ref.ReserveGetRaw();

    out->visible = false;
    out->SetWorldTransform(GLmat4_identity());
    out->SetVisible(true);

	return out;
}
void s3dDestroyObject (s3dObject* object)
{
    object->SetVisible(false);

	if (object->model) object->model->Abandon();
	s3dObjects.Remove(object);
	s3dObjectAllocator.Release(object);
}



s3dVec3 s3dCamera::GetNormal ()
{
    GLmat4 rotation = GLmat4_yrotate(slDegToRad_F(yaw)) * GLmat4_xrotate(slDegToRad_F(pitch));
	return rotation * GLvec4(0,0,1);
}

s3dCamera s3dMainCamera;
s3dCamera* s3dGetMainCamera ()
{
	return &s3dMainCamera;
};

void s3dGenCamMatrix (slInt2 target_wh, s3dCamera* cam)
{
	/// Camera matrix generation.
	// Account for non-square window aspect ratio.
	GLfloat aspect_x_mul,aspect_y_mul;
	if (target_wh.w > target_wh.h)
	{
		aspect_x_mul = target_wh.h / (GLfloat)target_wh.w;
		aspect_y_mul = 1;
	}
	else
	{
		aspect_x_mul = 1;
		aspect_y_mul = target_wh.w / (GLfloat)target_wh.h;
	};
	cam->final_matrix =
		GLmat4_scaling(GLvec4(aspect_x_mul,aspect_y_mul,1))
	  * cam->projection_matrix
		//GLmat4_ortho(-1,1,-1,1,-20,20)
		//GLmat4_perspective(90,0.1,1000)
	  * GLmat4_zrotate(slDegToRad_F(-cam->roll))
	  * GLmat4_xrotate(slDegToRad_F(-cam->pitch))
	  * GLmat4_yrotate(slDegToRad_F(-cam->yaw))
	  * GLmat4_offset(GLvec4(-cam->origin))
	;
};
void s3dInitShaders ();
void s3dQuitShaders ();
slBU s3dInitCount = 0;
slGC_Action s3dModelsGC_Action;
GLuint s3dVertexArray;

/*void s3dChaseTransformInherit (s3dObject* level, GLmat4& transform, GLvec4& offset)
{
	if (level->transform_inherit)
	{
		s3dChaseTransformInherit(level->transform_inherit,transform,offset);
		transform *= level->world_transform;
		offset += level->world_offset;
	}
	else
	{
		transform = level->world_transform;
		offset = level->world_offset;
	};
};*/

slShaderProgram s3dProgram;
GLuint
    s3dProgram_CameraMatrix,

    s3dProgram_CameraPos,
    s3dProgram_PhongEnabled,
    s3dProgram_LightPos,
    s3dProgram_LightIsOrtho,
    s3dProgram_LightDir,
    s3dProgram_MaterialAmbientColor,
    s3dProgram_MaterialDiffuseColor,
    s3dProgram_MaterialSpecularColor,
    s3dProgram_MaterialSpecularExponent,
    s3dProgram_EnvironmentAmbientColor,
    s3dProgram_IgnoreEnvironmentAmbient,
    s3dProgram_LightColor,
    s3dProgram_LightIsDirectional,
    s3dProgram_DirectionalLightDivide,
    s3dProgram_LightStrength,

    s3dProgram_DistanceFogEnabled,
    s3dProgram_DistanceFogColor,
    s3dProgram_DistanceFogMultiplier,

    s3dProgram_DiffuseVoxelingEnabled,
    s3dProgram_DiffuseVoxelingSize,
    s3dProgram_SpecularVoxelingEnabled,
    s3dProgram_SpecularVoxelingSize,
    s3dProgram_FogVoxelingEnabled,
    s3dProgram_FogVoxelingSize;

slTexture* s3dWhiteTexture;
inline slForceInline slTexture* s3dTexOrWhite (slTexture* texref)
{
    if (!texref) return s3dWhiteTexture;
    if (!texref->ready) return s3dWhiteTexture;
    return texref;
}
slTexture* s3dUpVecTexture;
inline slForceInline slTexture* s3dTexOrUpVec (slTexture* texref)
{
    if (!texref) return s3dUpVecTexture;
    if (!texref->ready) return s3dUpVecTexture;
    return texref;
}
GLfloat s3dTextureAnisotropicValue;
inline slForceInline void s3dUseTex (int texid, slTexture* texref, int mag_filter)
{
    glActiveTexture(GL_TEXTURE0 + texid);
    glBindTexture(GL_TEXTURE_2D,texref->tex);
    slSetTextureClamping(GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,mag_filter);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAX_ANISOTROPY_EXT,s3dTextureAnisotropicValue);
}
void s3dRender (slInt2 target_dims, s3dCamera* cam)
{
    s3dGenCamMatrix(target_dims, cam);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	glUseProgram(s3dProgram.program);
	glUniformMatrix4fv(s3dProgram_CameraMatrix,1,GL_TRUE,cam->final_matrix.data);
	glUniform3f(s3dProgram_CameraPos,cam->origin.x,cam->origin.y,cam->origin.z);

	glBindVertexArray(s3dVertexArray);

    for (slBU model_i = 0; model_i < s3dModels.itemcount; model_i++)
    {
        s3dModel* model = s3dModels.items[model_i];

        /*  Don't bother to draw if there are no transforms.
            (Models with no transforms aren't necessarily doomed for garbage
            collection, something may reference them but be set as invisible
            or have a temporary reference in some thread etc.)  */
        if (model->matrix_count)
        {
            glActiveTexture(GL_TEXTURE0 + 5);
            glBindTexture(GL_TEXTURE_BUFFER,model->matrix_texture);

			s3dMesh* mesh = model->meshes;
			for (uint32_t mesh_i = 0; mesh_i < model->meshcount; mesh_i++, mesh++)
			{
                slTexture* ambient_texref = s3dTexOrWhite(mesh->ambient_texref);
                slTexture* diffuse_texref = s3dTexOrWhite(mesh->diffuse_texref);
                slTexture* specular_texref = s3dTexOrWhite(mesh->specular_texref);
                slTexture* opacity_texref = s3dTexOrWhite(mesh->opacity_texref);
                slTexture* normal_texref = s3dTexOrUpVec(mesh->normal_texref);

                glUniform1i(s3dProgram_IgnoreEnvironmentAmbient,mesh->ignore_environment_ambient);
                glUniform3fv(s3dProgram_MaterialAmbientColor,1,(GLfloat*)&mesh->ambient_color);
                glUniform3fv(s3dProgram_MaterialDiffuseColor,1,(GLfloat*)&mesh->diffuse_color);
                glUniform3fv(s3dProgram_MaterialSpecularColor,1,(GLfloat*)&mesh->specular_color);
                glUniform1f(s3dProgram_MaterialSpecularExponent,mesh->specular_exponent);

                int mag_filter = mesh->mag_filter_nearest ? GL_NEAREST : GL_LINEAR;
                s3dUseTex(0,ambient_texref,mag_filter);
                s3dUseTex(1,diffuse_texref,mag_filter);
                s3dUseTex(2,specular_texref,mag_filter);
                s3dUseTex(3,opacity_texref,mag_filter);
                s3dUseTex(4,normal_texref,mag_filter);

				glBindBuffer(GL_ARRAY_BUFFER,mesh->vertex_buffer);
            	void* offset = 0;
            	size_t vertsize = sizeof(float) * 8;
            	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,vertsize,offset); offset += sizeof(GLfloat) * 3;
            	glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,vertsize,offset); offset += sizeof(GLfloat) * 2;
            	glVertexAttribPointer(2,3,GL_FLOAT,GL_FALSE,vertsize,offset); offset += sizeof(GLfloat) * 3;

				glDrawArraysInstanced(GL_TRIANGLES,0,mesh->triangle_count * 3,model->matrix_count);
			};
        }
    }

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
};
void s3dRender (slTargetTexRef target, s3dCamera* cam)
{
    target.DrawTo();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    s3dRender(target.GetDims(),cam);
}
void s3dRender (s3dCamera* cam)
{
    slDrawToScreen();
	glClear(GL_DEPTH_BUFFER_BIT);
    s3dRender(slGetWindowResolution(),cam);
}

char* s3dVertSrc = R"(#version 140

in vec3 RawVertPos;
uniform samplerBuffer TransformsTex;

out vec3 FragWorldPos;
uniform mat4 CameraMatrix;

in vec2 VertTexCoords;
out vec2 FragTexCoords;

in vec3 VertNormal;
out vec3 FragNormal;

void main ()
{
    int lookup_pos = gl_InstanceID * 4;
    mat4 ModelViewMatrix = mat4(
        texelFetch(TransformsTex, lookup_pos),
        texelFetch(TransformsTex, lookup_pos + 1),
        texelFetch(TransformsTex, lookup_pos + 2),
        texelFetch(TransformsTex, lookup_pos + 3)
    );
    if (false)
    {
        ModelViewMatrix = mat4(
            vec4(.01,0,0,0),
            vec4(0,.01,0,0),
            vec4(0,0,.01,0),
            vec4(0,0,0,1)
        );
    }

	vec4 vertpos = vec4(RawVertPos,1);
	vertpos = ModelViewMatrix * vertpos;
    FragWorldPos = vertpos.xyz;

	vertpos = CameraMatrix * vertpos;
	gl_Position = vertpos;
	FragTexCoords = VertTexCoords;

    FragNormal = mat3(ModelViewMatrix) * VertNormal;
}
)";

char* s3dFragSrc = R"(#version 130

in vec3 FragWorldPos;
in vec2 FragTexCoords;
in vec3 FragNormal;

uniform sampler2D AmbientTex;
uniform sampler2D DiffuseTex;
uniform sampler2D SpecularTex;
uniform sampler2D OpacityTex;
uniform sampler2D NormalTex;

uniform vec3 CameraPos;
uniform bool PhongEnabled;
uniform vec3 LightPos;
uniform bool LightIsOrtho;
uniform vec3 LightDir;
uniform vec3 MaterialAmbientColor;
uniform vec3 MaterialDiffuseColor;
uniform vec3 MaterialSpecularColor;
uniform float MaterialSpecularExponent;
uniform vec3 EnvironmentAmbientColor;
uniform bool IgnoreEnvironmentAmbient;
uniform vec3 LightColor;
uniform bool LightIsDirectional;
uniform float DirectionalLightDivide;
uniform float LightStrength;

uniform bool DistanceFogEnabled;
uniform vec3 DistanceFogColor;
uniform float DistanceFogMultiplier;

uniform bool FogVoxelingEnabled;
uniform float FogVoxelingSize;
uniform bool DiffuseVoxelingEnabled;
uniform float DiffuseVoxelingSize;
uniform bool SpecularVoxelingEnabled;
uniform float SpecularVoxelingSize;

mat3 CotangentFrame (vec3 Normal, vec3 position, vec2 texcoord)
{
    // get edge vectors of the pixel triangle
    vec3 diff_position_x = dFdx(position);
    vec3 diff_position_y = dFdy(position);
    vec2 diff_texcoord_x = dFdx(texcoord);
    vec2 diff_texcoord_y = dFdy(texcoord);

    // solve the linear system
    vec3 dp2perp = cross(diff_position_y, Normal);
    vec3 dp1perp = cross(Normal, diff_position_x);
    vec3 Tangent =
        dp2perp * diff_texcoord_x.x +
        dp1perp * diff_texcoord_y.x;
    vec3 Bitangent =
        dp2perp * diff_texcoord_x.y +
        dp1perp * diff_texcoord_y.y;

    return mat3(normalize(Tangent), normalize(Bitangent), Normal);
}

vec3 TransformByNormal (vec3 normal_from_map)
{
    mat3 transform_by_normal = CotangentFrame(normalize(FragNormal), -FragWorldPos, FragTexCoords);
    return normalize(transform_by_normal * normal_from_map);
}

void main ()
{
    float opacity = texture(OpacityTex,FragTexCoords).r;
    if (opacity == 0) discard;

    gl_FragColor.a = 1;
    vec3 ambient_tex_color = texture(AmbientTex,FragTexCoords).rgb;

    vec3 adjusted_world_pos;
    vec3 before_flooring = FragWorldPos - normalize(FragNormal)*.01; // trying to prevent weird chattering when exactly at a voxel boundary

    if (PhongEnabled)
    {
        vec3 diffuse_tex_color = texture(DiffuseTex,FragTexCoords).rgb;
        vec3 specular_tex_color = texture(SpecularTex,FragTexCoords).rgb;

        gl_FragColor.rgb = MaterialAmbientColor * ambient_tex_color;
        if (!IgnoreEnvironmentAmbient) gl_FragColor.rgb *= EnvironmentAmbientColor;

        if (DiffuseVoxelingEnabled) adjusted_world_pos = (floor(before_flooring / DiffuseVoxelingSize)+.5) * DiffuseVoxelingSize;
        else adjusted_world_pos = FragWorldPos;
        vec3 light_dir;
        if (LightIsOrtho) light_dir = normalize(LightPos);
        else light_dir = normalize(LightPos - adjusted_world_pos);

        vec3 normal = texture(NormalTex,FragTexCoords).rgb * 2 - 1;
        normal = TransformByNormal(normal);

        float diff = max(dot(light_dir, normal), 0);
        vec3 diffuse = diff * MaterialDiffuseColor * diffuse_tex_color;

        if (SpecularVoxelingEnabled) adjusted_world_pos = (floor(before_flooring / SpecularVoxelingSize)+.5) * SpecularVoxelingSize;
        else adjusted_world_pos = FragWorldPos;
        // have to recalculate this in case different voxeling was used.
        if (!LightIsOrtho) normalize(LightPos - adjusted_world_pos);

        vec3 view_dir = normalize(CameraPos - adjusted_world_pos);
        vec3 halfway_dir = normalize(light_dir + view_dir);
        float spec_value = pow(max(dot(normal,halfway_dir),0),MaterialSpecularExponent);
        vec3 specular = MaterialSpecularColor * spec_value * specular_tex_color;

        float directional_light_diff;
        if (LightIsDirectional) directional_light_diff = max(dot(light_dir, LightDir) - (1 - DirectionalLightDivide), 0) / DirectionalLightDivide;
        else directional_light_diff = 1;

        float distance_from_light;
        if (LightIsOrtho) distance_from_light = 0;
        else distance_from_light = length(LightPos - FragWorldPos);
        float light_intensity = exp(distance_from_light / LightStrength);
        light_intensity = clamp(light_intensity,0,1);

        gl_FragColor.rgb += (diffuse + specular) * directional_light_diff * LightColor * light_intensity;
    }
    else gl_FragColor.rgb = ambient_tex_color * MaterialAmbientColor;

    if (DistanceFogEnabled)
    {
        if (FogVoxelingEnabled) adjusted_world_pos = (floor(before_flooring / FogVoxelingSize)+.5) * FogVoxelingSize;
        else adjusted_world_pos = FragWorldPos;

	    float DistanceFromCamera = length(adjusted_world_pos - CameraPos);
        float bright = exp(DistanceFromCamera * DistanceFogMultiplier);
        bright = clamp(bright,0,1);
        gl_FragColor.rgb *= bright;
        gl_FragColor.rgb += DistanceFogColor * (1 - bright);
    }
}
)";

void s3dBindAttribs (GLuint program)
{
	glBindAttribLocation(program,0,"RawVertPos");
	glBindAttribLocation(program,1,"VertTexCoords");
	glBindAttribLocation(program,2,"VertNormal");
};
void s3dInitShaders ()
{
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT,&s3dTextureAnisotropicValue);

    s3dWhiteTexture = slSolidColorTexture({0xFF,0xFF,0xFF,0xFF}).ReserveGetRaw();
    s3dUpVecTexture = slSolidColorTexture({0x80,0x80,0xFF,0xFF}).ReserveGetRaw();

	s3dProgram = slCreateShaderProgram("Slice3D Shaders",s3dVertSrc,s3dFragSrc,s3dBindAttribs);

	s3dProgram_CameraMatrix = slLocateUniform(s3dProgram,"CameraMatrix");

    glUniform1i(slLocateUniform(s3dProgram,"AmbientTex"),0);
	glUniform1i(slLocateUniform(s3dProgram,"DiffuseTex"),1);
    glUniform1i(slLocateUniform(s3dProgram,"SpecularTex"),2);
    glUniform1i(slLocateUniform(s3dProgram,"OpacityTex"),3);
    glUniform1i(slLocateUniform(s3dProgram,"NormalTex"),4);
    glUniform1i(slLocateUniform(s3dProgram,"TransformsTex"),5);

    s3dProgram_CameraPos = slLocateUniform(s3dProgram,"CameraPos");
    s3dProgram_PhongEnabled = slLocateUniform(s3dProgram,"PhongEnabled");
    s3dProgram_LightPos = slLocateUniform(s3dProgram,"LightPos");
    s3dProgram_LightIsOrtho = slLocateUniform(s3dProgram,"LightIsOrtho");
    s3dProgram_LightDir = slLocateUniform(s3dProgram,"LightDir");
    s3dProgram_MaterialAmbientColor = slLocateUniform(s3dProgram,"MaterialAmbientColor");
    s3dProgram_MaterialDiffuseColor = slLocateUniform(s3dProgram,"MaterialDiffuseColor");
    s3dProgram_MaterialSpecularColor = slLocateUniform(s3dProgram,"MaterialSpecularColor");
    s3dProgram_MaterialSpecularExponent = slLocateUniform(s3dProgram,"MaterialSpecularExponent");
    s3dProgram_EnvironmentAmbientColor = slLocateUniform(s3dProgram,"EnvironmentAmbientColor");
    s3dProgram_IgnoreEnvironmentAmbient = slLocateUniform(s3dProgram,"IgnoreEnvironmentAmbient");
    s3dProgram_LightColor = slLocateUniform(s3dProgram,"LightColor");
    s3dProgram_LightIsDirectional = slLocateUniform(s3dProgram,"LightIsDirectional");
    s3dProgram_DirectionalLightDivide = slLocateUniform(s3dProgram,"DirectionalLightDivide");
    s3dProgram_LightStrength = slLocateUniform(s3dProgram,"LightStrength");

    s3dProgram_DistanceFogEnabled = slLocateUniform(s3dProgram,"DistanceFogEnabled");
    s3dProgram_DistanceFogColor = slLocateUniform(s3dProgram,"DistanceFogColor");
    s3dProgram_DistanceFogMultiplier = slLocateUniform(s3dProgram,"DistanceFogMultiplier");

    s3dProgram_DiffuseVoxelingEnabled = slLocateUniform(s3dProgram,"DiffuseVoxelingEnabled");
    s3dProgram_DiffuseVoxelingSize = slLocateUniform(s3dProgram,"DiffuseVoxelingSize");
    s3dProgram_SpecularVoxelingEnabled = slLocateUniform(s3dProgram,"SpecularVoxelingEnabled");
    s3dProgram_SpecularVoxelingSize = slLocateUniform(s3dProgram,"SpecularVoxelingSize");
    s3dProgram_FogVoxelingEnabled = slLocateUniform(s3dProgram,"FogVoxelingEnabled");
    s3dProgram_FogVoxelingSize = slLocateUniform(s3dProgram,"FogVoxelingSize");
};
void s3dQuitShaders ()
{
    s3dWhiteTexture->Abandon();
    s3dUpVecTexture->Abandon();

	slDestroyShaderProgram(s3dProgram);
};
void s3dSetDistanceFog (bool enabled, GLcolor3 rgb, GLfloat multiplier)
{
    glUseProgram(s3dProgram.program);
    glUniform1i(s3dProgram_DistanceFogEnabled,enabled);
    glUniform3fv(s3dProgram_DistanceFogColor,1,(GLfloat*)&rgb);
    glUniform1f(s3dProgram_DistanceFogMultiplier,-multiplier);
}
void s3dSetLightPos (bool phong_enabled, bool ortho_mode, GLvec4 light_xyz)
{
    glUseProgram(s3dProgram.program);
    glUniform1i(s3dProgram_PhongEnabled,phong_enabled);
    glUniform3fv(s3dProgram_LightPos,1,(GLfloat*)&light_xyz.data);
    glUniform1i(s3dProgram_LightIsOrtho,ortho_mode);
}
void s3dSetLightDir (bool is_directional, GLvec4 light_dir, slScalar field_angle)
{
    glUseProgram(s3dProgram.program);
    glUniform1i(s3dProgram_LightIsDirectional,is_directional);
    glUniform3fv(s3dProgram_LightDir,1,(GLfloat*)&light_dir.data);
    glUniform1f(s3dProgram_DirectionalLightDivide, field_angle / 180);
}
void s3dSetEnvironmentAmbientColor (GLcolor3 color)
{
    glUseProgram(s3dProgram.program);
    glUniform3fv(s3dProgram_EnvironmentAmbientColor,1,(GLfloat*)&color);
}
void s3dSetLightColor (GLcolor3 color)
{
    glUseProgram(s3dProgram.program);
    glUniform3fv(s3dProgram_LightColor,1,(GLfloat*)&color);
}
void s3dSetLightStrength (GLfloat strength)
{
    glUseProgram(s3dProgram.program);
    glUniform1f(s3dProgram_LightStrength,-strength);
}
void s3dSetDiffuseVoxeling (bool voxeling_enabled, GLfloat voxel_size)
{
    glUseProgram(s3dProgram.program);
    glUniform1i(s3dProgram_DiffuseVoxelingEnabled,voxeling_enabled);
    glUniform1f(s3dProgram_DiffuseVoxelingSize,voxel_size);
}
void s3dSetSpecularVoxeling (bool voxeling_enabled, GLfloat voxel_size)
{
    glUseProgram(s3dProgram.program);
    glUniform1i(s3dProgram_SpecularVoxelingEnabled,voxeling_enabled);
    glUniform1f(s3dProgram_SpecularVoxelingSize,voxel_size);
}
void s3dSetFogVoxeling (bool voxeling_enabled, GLfloat voxel_size)
{
    glUseProgram(s3dProgram.program);
    glUniform1i(s3dProgram_FogVoxelingEnabled,voxeling_enabled);
    glUniform1f(s3dProgram_FogVoxelingSize,voxel_size);
}

SDL_Surface* s3dBumpToNormalMap (SDL_Surface* in_surf)
{
    /* TO DO: Use a GPU compute shader to do this work. */

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    SDL_Surface* surf = SDL_CreateRGBSurface(0,in_surf->w,in_surf->h,32,0xFF<<24,0xFF<<16,0xFF<<8,0xFF);
#else
    SDL_Surface* surf = SDL_CreateRGBSurface(0,in_surf->w,in_surf->h,32,0xFF,0xFF<<8,0xFF<<16,0xFF<<24);
#endif

    int total = in_surf->w * in_surf->h;
    Uint8* in_color_ptr = in_surf->pixels;
    Uint8* out_color_ptr = surf->pixels;
    for (int y = 0; y < in_surf->h; y++)
    {
        int next_y = (y + 1) % in_surf->h;
        int off_y = (next_y - y) * in_surf->w * 4;
        for (int x = 0 ; x < in_surf->w; x++)
        {
            int next_x = (x + 1) % in_surf->w;
            int off_x = (next_x - x) * 4;

            s3dVec3 pos_here = s3dVec3(x, y, *in_color_ptr / -(slScalar)0x3F);
            s3dVec3 pos_right = s3dVec3(x + 1, y, *(in_color_ptr + off_x) / -(slScalar)0x3F);
            s3dVec3 pos_below = s3dVec3(x, y + 1, *(in_color_ptr + off_y) / -(slScalar)0x3F);
            in_color_ptr += 4;

            s3dVec3 normal = cross(pos_right - pos_here, pos_below - pos_here).normalized();

            *out_color_ptr++ = 0x80 + (int)(normal.x * 0x7F);
            *out_color_ptr++ = 0x80 + (int)(normal.y * 0x7F);
            *out_color_ptr++ = 0x80 + (int)(normal.z * 0x7F);
            *out_color_ptr++ = 0xFF;
        }
    }

    return surf;
}

struct s3dBumpTexturesTreeProto : slTreeMap<s3dBumpInfo,s3dBumpTexture>
{
    s3dBumpTexturesTreeProto ()
        : slTreeMap(&s3dBumpTexture::bump_tree_key,&s3dBumpTexture::bump_tree_data)
    {}
    int Compare (s3dBumpInfo* info_a, s3dBumpInfo* info_b) override
    {
        /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */
        return strcmp(info_a->filepath,info_b->filepath);
    }
    void PrintKey (s3dBumpInfo* info) override
    {
        printf("bump '%s'\n",info->filepath);
    }
}
s3dBumpTexturesTree;

s3dBumpTexture::s3dBumpTexture (s3dBumpInfo search_key) : bump_tree_key(search_key)
{
    bump_tree_key.filepath = slStrAlwaysClone(bump_tree_key.filepath);
    s3dBumpTexturesTree.Insert(this);
    //printf("count:   %d\n",s3dBumpTexturesTree.total_items);
    //printf("recount: %d\n",s3dBumpTexturesTree.Recount());
    //s3dBumpTexturesTree.PrintInOrder();
}
s3dBumpTexture::~s3dBumpTexture ()
{
    free(bump_tree_key.filepath);
}
void s3dBumpTexture::Unregister ()
{
    slTexture::Unregister();
    s3dBumpTexturesTree.Remove(this);
    //printf("count:   %d\n",s3dBumpTexturesTree.total_items);
    //printf("recount: %d\n",s3dBumpTexturesTree.Recount());
    //s3dBumpTexturesTree.PrintInOrder();
}

extern SDL_Surface* slInvalidTextureSurf;
extern SDL_mutex* slTexListMutex;
class s3dBumpAsyncQueueProto : public slConsumerQueue
{
public:
    s3dBumpAsyncQueueProto () : slConsumerQueue(offsetof(s3dBumpTexture,next_load)) {};
private:
    void ItemFunc (void* queue_item) override
    {
        s3dBumpTexture* out_tex = queue_item;

    	//printf("bump texture is loading\n");
    	//SDL_Delay(500); // Simulate very slow disk.

        SDL_Surface* original = slLoadTexture_Surf(out_tex->bump_tree_key.filepath);
    	SDL_Surface* converted;
    	if (original)
    	{
            converted = slConvertSurface_RGBA32(original);
    		SDL_FreeSurface(original);
        }
        else converted = NULL;

        SDL_Surface* normals_surf;
        if (converted)
        {
            normals_surf = s3dBumpToNormalMap(converted);
            SDL_FreeSurface(converted);
        }
        else
        {
            normals_surf = slInvalidTextureSurf;
            printf("failure to load bump map from '%s'\n",out_tex->bump_tree_key.filepath);
        }

        slQueueTexUpload(out_tex,normals_surf);
    }
}
s3dBumpAsyncQueue;

s3dBumpTexRef s3dLoadBumpMap (char* path)
{
    SDL_LockMutex(slTexListMutex);

    s3dBumpInfo search_key(path);
    s3dBumpTexture* found_tex = s3dBumpTexturesTree.Lookup(&search_key);
    if (found_tex)
    {
        s3dBumpTexRef ref_out = found_tex;
        SDL_UnlockMutex(slTexListMutex);
        return ref_out;
    }

    s3dBumpTexture* out_tex = new s3dBumpTexture(search_key);

    out_tex->Reserve();
    s3dBumpAsyncQueue.Push(out_tex);

    s3dBumpTexRef ref_out = out_tex;
    SDL_UnlockMutex(slTexListMutex);
	return ref_out;
}

void s3dObject::SetWorldTransform (GLmat4 world_transform)
{
    this->world_transform = world_transform.transpose();
    if (visible) model->UpdateMatrix(this);
}
void s3dObject::SetVisible (bool visible)
{
    if (visible != this->visible)
    {
        this->visible = visible;
        if (visible) model->AddMatrix(this);
        else model->RemoveMatrix(this);
    }
}
void s3dObject::SetModel (s3dModel* set_to)
{
	if (model)
    {
        /* Take our transform out of this model. */
        if (visible) model->RemoveMatrix(this);

        model->Abandon();
    }
	model = set_to;
	if (model)
    {
        model->Reserve();

        /* Put our transform in this model. */
        if (visible) model->AddMatrix(this);
    }
}


void s3dInit ()
{
	if (s3dInitCount++) return;

    s3dModelListMutex = SDL_CreateMutex();
    s3dObjLoaders.Init();

    s3dBumpAsyncQueue.Init();

	s3dInitShaders();

    glGenVertexArrays(1,&s3dVertexArray);
    glBindVertexArray(s3dVertexArray);
    glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	phys3dInit();
};
void s3dQuit ()
{
	if (--s3dInitCount) return;

    phys3dQuit();

    glDeleteVertexArrays(1,&s3dVertexArray);

	s3dQuitShaders();

	s3dObjects.UntilEmpty(s3dDestroyObject);
	s3dObjectAllocator.FreeAllBlocks();

    slNextFrameTasks.Flush();
    /*
        Destroying objects can cause transform updates to be generated.
        These updates reserve models until they have been completed, so
        we need to finish the updates in order for models to be freed.
        Otherwise we'll deadlock when we clean up models below.
        (s3dDoomModel will never actually doom a model, so UntilEmpty will
        keep trying it over and over infinitely).
    */

    s3dObjLoaders.Quit();
    slWaitBackgroundUploads(&s3dModelsInFinalization);

    // Can destroy existing models in current thread, but use s3dDoomModel
    // since doing anything differently is just a maintenance hazard.
	s3dModels.UntilEmpty(s3dDoomModel);
	// Wait until background destruction is finished.
    slWaitBackgroundUploads(&s3dModelsQueuedForGC);

    SDL_DestroyMutex(s3dModelListMutex);

    s3dBumpAsyncQueue.Quit();
};
