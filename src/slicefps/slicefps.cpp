#include <slice.h>
slBU fpsFrames = 0;
slScalar fpsElapsed = 0;
slBU fpsTotalFrames = 0;
slScalar fpsTotalElapsed = 0;
slScalar fpsCurrent = 0;
slBox* fpsIndicatorBox = NULL;
void fpsIndicatorUpdate ()
{
	/*static char text [5] = {'0','0','0','0',0};
	if (++text[3] > '9')
	{
		text[3] = '0';
		if (++text[2] > '9')
		{
			text[2] = '0';
			if (++text[1] > '9')
			{
				text[1] = '0';
				if (text[0] > '9')
				{
					text[0] = '0';
				};
			};
		};
	};
	//char* text = "Hello";*/
	char* text;
	asprintf(&text,"FPS: %u",(unsigned int)slRound(fpsCurrent));
	//printf("String: [%s]\n",text);
	//slQueueReplaceTexture(&fpsIndicatorBox->texref,slRenderText(text));
	slQueueTexSwap(&fpsIndicatorBox->texref_swapchain,slRenderText(text));
	free(text);
};
void fpsStep ()
{
	slScalar delta = slGetDelta();
	fpsFrames++;
	fpsElapsed += delta;
	fpsTotalFrames++;
	fpsTotalElapsed += delta;
	if (fpsElapsed >= 1)
	{
		fpsCurrent = fpsFrames / fpsElapsed;
		if (fpsIndicatorBox) fpsIndicatorUpdate();
		fpsFrames = 0;
		fpsElapsed = 0;
	};
};
slScalar fpsGetCurrent ()
{
	return fpsCurrent;
};
slScalar fpsGetTotal ()
{
	return fpsTotalFrames / fpsTotalElapsed;
};
slVec2 fpsIndicatorXY,fpsIndicatorWH;
void fpsCreateIndicator ()
{
	if (!fpsIndicatorBox)
	{
		fpsIndicatorBox = slCreateBox();
		fpsIndicatorBox->SetDims(fpsIndicatorXY,fpsIndicatorWH,0);
		fpsIndicatorBox->backcolor = {0,0,0,255};
		fpsIndicatorBox->bordercolor = {255,255,255,255};
		fpsIndicatorUpdate();
	};
};
void fpsDestroyIndicator ()
{
	if (fpsIndicatorBox)
	{
		//slCancelTextureReplacements(&fpsIndicatorBox->texref);
		slDestroyBox(fpsIndicatorBox);
		fpsIndicatorBox = NULL;
	};
};
bool fpsShowFPS = false;
void fpsSetIndicatorEnabled (bool to)
{
	fpsShowFPS = to;
	if (to) fpsCreateIndicator();
	else fpsDestroyIndicator();
};
void fpsToggleFPS ()
{
	fpsSetIndicatorEnabled(!fpsShowFPS);
};
slKeyBind* fpsBind;
void fpsInit (slVec2 indicator_xy, slVec2 indicator_wh)
{
	fpsIndicatorXY = indicator_xy;
	fpsIndicatorWH = indicator_wh;
	fpsBind = slGetKeyBind("Show/Hide Framerate",slKeyCode(SDLK_p));
	fpsBind->onpress = fpsToggleFPS;
	slHook_PreRender(fpsStep);
};
void fpsQuit ()
{
	fpsBind->onpress = NULL;
	fpsSetIndicatorEnabled(false);
	slUnhook_PreRender(fpsStep);
};
